


#include "massXiAlg/massXi.h" // header file


void massXi::MCtruth(SmartDataPtr<Event::McParticleCol> mcParticleCol){



	for (Event::McParticleCol::iterator itr = mcParticleCol->begin(); itr != mcParticleCol->end(); ++itr){

        int PID = (*itr)->particleProperty();

		Event::McParticle motherParticle = (*itr)->mother();
        int motherPID = motherParticle.particleProperty();
		
		HepLorentzVector p4itr = (*itr)->initialFourMomentum();

		if(PID==3312){//Xi
			
			mXip4_truth = p4itr.m();
			double MJPsi = 3.0969;
			mXi_truth = sqrt( (MJPsi/2)*(MJPsi/2) - p4itr.px()*p4itr.px() - p4itr.py()*p4itr.py() - p4itr.pz()*p4itr.pz() );
			DE_Xi_truth = MJPsi/2 - p4itr.e();
			
			HepLorentzVector finalVertex = (*itr)->finalPosition();
			HepLorentzVector initialVertex = (*itr)->initialPosition();
			HepLorentzVector lengthVector = finalVertex - initialVertex;

			decayLength_Xi_truth = sqrt(lengthVector.x()*lengthVector.x() + lengthVector.y()*lengthVector.y() + lengthVector.z()*lengthVector.z() );
			XiVertexX_truth = finalVertex.x();
			XiVertexY_truth = finalVertex.y();
			XiVertexZ_truth = finalVertex.z();
			XiVertexR_truth = sqrt(finalVertex.x()*finalVertex.x() + finalVertex.y()*finalVertex.y() + finalVertex.z()*finalVertex.z());

			XiPT_truth = sqrt(p4itr.px()*p4itr.px() + p4itr.py()*p4itr.py());
			XiPX_truth = p4itr.px();
			XiPY_truth = p4itr.py();
			XiPZ_truth = p4itr.pz();

		}
		
		if(PID==-3312){//Xibar
			
			mXibarp4_truth = p4itr.m();
			double MJPsi = 3.0969;
			mXibar_truth = sqrt( (MJPsi/2)*(MJPsi/2) - p4itr.px()*p4itr.px() - p4itr.py()*p4itr.py() - p4itr.pz()*p4itr.pz() );
			DE_Xibar_truth = MJPsi/2 - p4itr.e();
			
			HepLorentzVector finalVertex = (*itr)->finalPosition();
			HepLorentzVector initialVertex = (*itr)->initialPosition();
			HepLorentzVector lengthVector = finalVertex - initialVertex;

			decayLength_Xibar_truth = sqrt(lengthVector.x()*lengthVector.x() + lengthVector.y()*lengthVector.y() + lengthVector.z()*lengthVector.z() );
			XibarVertexX_truth = finalVertex.x();
			XibarVertexY_truth = finalVertex.y();
			XibarVertexZ_truth = finalVertex.z();
			XibarVertexR_truth = sqrt(finalVertex.x()*finalVertex.x() + finalVertex.y()*finalVertex.y() + finalVertex.z()*finalVertex.z());

			XibarPT_truth = sqrt(p4itr.px()*p4itr.px() + p4itr.py()*p4itr.py());
			XibarPX_truth = p4itr.px();
			XibarPY_truth = p4itr.py();
			XibarPZ_truth = p4itr.pz();

		}	
		
	
		if(PID==2212){//proton
			ProtonPPT_truth = sqrt(p4itr.px()*p4itr.px() + p4itr.py()*p4itr.py());
			ProtonPPX_truth = p4itr.px();
			ProtonPPY_truth = p4itr.py();
			ProtonPPZ_truth = p4itr.pz();
		}
		if(PID==-2212){//protonm
			ProtonMPT_truth = sqrt(p4itr.px()*p4itr.px() + p4itr.py()*p4itr.py());
			ProtonMPX_truth = p4itr.px();
			ProtonMPY_truth = p4itr.py();
			ProtonMPZ_truth = p4itr.pz();
		}

		if(PID==3122){//Lbd
			
			mLbd_truth = p4itr.m();
			
			HepLorentzVector finalVertex = (*itr)->finalPosition();
			HepLorentzVector initialVertex = (*itr)->initialPosition();
			HepLorentzVector lengthVector = finalVertex - initialVertex;

			decayLength_Lbd_truth = sqrt(lengthVector.x()*lengthVector.x() + lengthVector.y()*lengthVector.y() + lengthVector.z()*lengthVector.z() );
			LbdVertexX_truth = finalVertex.x();
			LbdVertexY_truth = finalVertex.y();
			LbdVertexZ_truth = finalVertex.z();
			LbdVertexR_truth = sqrt(finalVertex.x()*finalVertex.x() + finalVertex.y()*finalVertex.y() + finalVertex.z()*finalVertex.z());

			LbdPT_truth = sqrt(p4itr.px()*p4itr.px() + p4itr.py()*p4itr.py());
			LbdPX_truth = p4itr.px();
			LbdPY_truth = p4itr.py();
			LbdPZ_truth = p4itr.pz();

		}	
		if(PID==-3122){//Lbdbar
			
			mLbdbar_truth = p4itr.m();
			
			HepLorentzVector finalVertex = (*itr)->finalPosition();
			HepLorentzVector initialVertex = (*itr)->initialPosition();
			HepLorentzVector lengthVector = finalVertex - initialVertex;

			decayLength_Lbdbar_truth = sqrt(lengthVector.x()*lengthVector.x() + lengthVector.y()*lengthVector.y() + lengthVector.z()*lengthVector.z() );
			LbdbarVertexX_truth = finalVertex.x();
			LbdbarVertexY_truth = finalVertex.y();
			LbdbarVertexZ_truth = finalVertex.z();
			LbdbarVertexR_truth = sqrt(finalVertex.x()*finalVertex.x() + finalVertex.y()*finalVertex.y() + finalVertex.z()*finalVertex.z());

			LbdbarPT_truth = sqrt(p4itr.px()*p4itr.px() + p4itr.py()*p4itr.py());
			LbdbarPX_truth = p4itr.px();
			LbdbarPY_truth = p4itr.py();
			LbdbarPZ_truth = p4itr.pz();

		}	
	
		if(PID==211&&motherPID==-3312){//pionp from Xibar
			PionPXiPT_truth = sqrt(p4itr.px()*p4itr.px() + p4itr.py()*p4itr.py());
			PionPXiPX_truth = p4itr.px();
			PionPXiPY_truth = p4itr.py();
			PionPXiPZ_truth = p4itr.pz();
		}
		if(PID==211&&motherPID==-3122){//pionp from lbdbar 
			PionPLbdPT_truth = sqrt(p4itr.px()*p4itr.px() + p4itr.py()*p4itr.py());
			PionPLbdPX_truth = p4itr.px();
			PionPLbdPY_truth = p4itr.py();
			PionPLbdPZ_truth = p4itr.pz();
		}
		
		if(PID==-211&&motherPID==3312){//pionm from Xi
			PionMXiPT_truth = sqrt(p4itr.px()*p4itr.px() + p4itr.py()*p4itr.py());
			PionMXiPX_truth = p4itr.px();
			PionMXiPY_truth = p4itr.py();
			PionMXiPZ_truth = p4itr.pz();
		}
		if(PID==-211&&motherPID==3122){//pionm from lbd 
			PionMLbdPT_truth = sqrt(p4itr.px()*p4itr.px() + p4itr.py()*p4itr.py());
			PionMLbdPX_truth = p4itr.px();
			PionMLbdPY_truth = p4itr.py();
			PionMLbdPZ_truth = p4itr.pz();
		}
		
	}



}






void massXi::MCtopo(SmartDataPtr<Event::McParticleCol> mcParticleCol){

	Event::McParticleCol::iterator iter_mc_begin = mcParticleCol->begin();
	Event::McParticleCol::iterator iter_mc_end = mcParticleCol->end();
	Event::McParticleCol::iterator iter_mc = mcParticleCol->begin();

	//refresh:
	for(int i=0;i<100;i++){
		m_pdgid[i]=0;
		m_motheridx[i]=0;
	}
	m_numParticle=0;


	std::vector<int> OriginIndex;	// MCTruth NoteDown As Topology Layout
	OriginIndex.clear();
	int PNum=0;
	m_numParticle=1;      
	m_pdgid[0]=91;	// Virtual Mother Particle e+e-
	m_motheridx[0]=-1;    
	OriginIndex.push_back(-1);    
	for(PNum = 0; PNum < m_numParticle; PNum++){
		for(iter_mc = iter_mc_begin; iter_mc != mcParticleCol->end(); iter_mc++ ){
			int mmm;
			mmm = ((*iter_mc)->mother()).mother().mother().mother().particleProperty(); 
			if(PNum == 0){
				//When Mother Is 'cluster' Or 'string' Start NoteDown
				if( ( (*iter_mc)->mother() ).particleProperty()==91 || ( (*iter_mc)->mother() ).particleProperty()==92 ){
					OriginIndex.push_back((*iter_mc)->trackIndex());
					m_pdgid[m_numParticle]=(*iter_mc)->particleProperty();
					m_motheridx[m_numParticle]=0;
					m_numParticle++;
				}
			}else if( ( (*iter_mc)->mother() ).trackIndex()  == OriginIndex[PNum] ){
				//When Mother's Mother's Mother Is 'cluster' Or 'string' Stop NoteDown, Modded By Variable int mmm
				//e.g. e+e- -> L+L-, L+ -> P+K_S0, L- -> NPi-. Only Second Rank And Precedent Decay Product Will Be Noted
				if( mmm!=91 && mmm!=92 ){
					OriginIndex.push_back((*iter_mc)->trackIndex());
				}else{
					OriginIndex.push_back(9999);
				}
				m_pdgid[m_numParticle]=(*iter_mc)->particleProperty();
				m_motheridx[m_numParticle]=PNum;
				m_numParticle++;
			}       
		}               
	}

}










