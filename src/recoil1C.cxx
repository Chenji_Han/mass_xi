/*************************************************************************
    > File Name: recoil1C.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Sun 16 Feb 2020 02:59:20 PM CST
 ************************************************************************/

#include "massXiAlg/massXi.h"

double string2double(string input){
	
	int index = 0;
	double pre = 0.0;
	double aft = 0.0;
	bool comma = false;
	double num = 1.0;
	double charge;
	if(input[0]=='-') 
		charge = -1.0;
	else 
		charge = 1.0;

	while(input[index]!='\0'){
		if(input[index]=='.'){
			comma = true;
		}
		if('0'<=input[index]&&input[index]<='9'){
			double temp = input[index] - '0';
			if(comma==false){
				pre = pre*10.0 + temp;
			}else{
				num *= 10.0;
				aft += temp/num;
			}
		}
		index++;
	}

	return charge * (pre+aft) ;

}


//double massXi::recoil1C ( WTrackParameter wXi, double Etotal, HepLorentzVector& P4_Xi_recoil1C ){
//
//    double recoilResonance;
//
//    string line;
//    std::ifstream inputTxt("inputs.txt");
//    while(getline(inputTxt, line)) {
//        if (!line.length() || line[0] == '#'){
//            continue;
//        }
//    	std::istringstream iss(line);
//            string a,b,c;
//            iss>>a>>b>>c;
//            if(a=="dst_recoil1C"){
//    	        recoilResonance = string2double(b);
//            }
//    }
//
//    if( recoilResonance == 0 ){ 
//        P4_Xi_recoil1C = HepLorentzVector( 0, 0, 0, 0 ); 
//        return 9999;                       
//    }
//
//    KalmanKinematicFit *m_kmfit;
//    m_kmfit = KalmanKinematicFit::instance();
//    m_kmfit->init();
//
//    m_kmfit->AddTrack( 0, wXi );
//    m_kmfit->AddMissTrack( 1, recoilResonance );
//    m_kmfit->AddFourMomentum( 0, HepLorentzVector(0, 0, 0, Etotal) );
//
//    if( !m_kmfit->Fit(0) ){ 
//        P4_Xi_recoil1C = HepLorentzVector( 0, 0, 0, 0 ); 
//        return 9999;                       
//    }
//   
//    m_kmfit->BuildVirtualParticle(0);
//    P4_Xi_recoil1C = m_kmfit->pfit(0);
//
//    return m_kmfit->chisq(0);
//
//}
