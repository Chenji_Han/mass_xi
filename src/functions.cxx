/*************************************************************************
    > File Name: functions.cxx
    > Author: Chenji Han
    > Mail: hanchenji98@gmail.com 
    > Created Time: Tue 17 Sep 2019 07:33:55 PM CST
 ************************************************************************/

#include "massXiAlg/massXi.h"
//extern int N3;

bool massXi::angleMatch(int MotherId,vector<int> raw,vector<int> raw_id){
		
		SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
		SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), EventModel::EvtRec::EvtRecEvent);
		SmartDataPtr<EvtRecTrackCol> evtRecTrkCol(eventSvc(),  EventModel::EvtRec::EvtRecTrackCol);

		vector<const EvtRecTrack*> tracks;
		for(int i=0;i<raw.size();i++){
			const EvtRecTrack* itTrk = *( evtRecTrkCol->begin() + raw[i] );
			tracks.push_back(itTrk);
		}

		return matchSvc->match(tracks,raw_id,MotherId);

}



int massXi::IsGoodTrack(SmartDataPtr<EvtRecEvent> evtRecEvent,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,vector<HepLorentzVector>& PionP,  vector<HepLorentzVector>& PionM,
																										 vector<HepLorentzVector>& ProtonP,vector<HepLorentzVector>& ProtonM,
																										 vector<int>& PionP_index,vector<int>& PionM_index,
																										 vector<int>& ProtonP_index,vector<int>& ProtonM_index,int& nProtonP,int& nProtonM,int& nPionP,int& nPionM){

    int TrackNumber = 0;
	nProtonP = 0;
	nProtonM = 0;
	nPionM = 0;
	nPionP = 0;

	Hep3Vector xorigin(0,0,0);

	IVertexDbSvc*  vtxsvc;
	Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);
	if(vtxsvc->isVertexValid()){
		double* dbv = vtxsvc->PrimaryVertex(); 
		double*  vv = vtxsvc->SigmaPrimaryVertex();  
		xorigin.setX(dbv[0]);
		xorigin.setY(dbv[1]);
		xorigin.setZ(dbv[2]);
	}else{
		cout<<"invalid vertex"<<endl;
		return 0;
	}

	for(int i = 0; i < evtRecEvent->totalCharged(); i++){

		EvtRecTrackIterator itTrk=evtRecTrkCol->begin() + i;
		
		if(!(*itTrk)->isMdcTrackValid()) continue;
		if(!(*itTrk)->isMdcKalTrackValid()) continue;

		RecMdcTrack *mdcTrk = (*itTrk)->mdcTrack();
		HepVector a = mdcTrk->helix();
		HepSymMatrix Ea = mdcTrk->err();
		HepPoint3D point0(0.,0.,0.);   // the initial point for MDC recosntruction
		HepPoint3D IP(xorigin[0],xorigin[1],xorigin[2]); 
		VFHelix helixip(point0,a,Ea); 
		helixip.pivot(IP);
		HepVector vecipa = helixip.a();
		double  Rvxy0=fabs(vecipa[0]);  //the nearest distance to IP in xy plane
		double  Rvz0=vecipa[3];         //the nearest distance to IP in z direction

		double costheta = cos( mdcTrk->theta() );
		if(!(abs(costheta)<0.93)) continue;


//		if(z0_cut>0){
//			if(fabs(Rvz0) >= z0_cut) continue;
//		}
//
//		if(r0_cut>0){
//			if(fabs(Rvxy0) >= r0_cut) continue;
//		}

        TrackNumber++;
        
		RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();

		ParticleID * pid=ParticleID::instance();
		double m_prob_pid_pi,m_prob_pid_k,m_prob_pid_p;
	    pid->init();
	    pid->setMethod(pid->methodProbability());
	    pid->setChiMinCut(8);
	    pid->setRecTrack(*itTrk);
	    pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTof()); // use PID sub-system
	    pid->identify(pid->onlyProton() | pid->onlyPion() | pid->onlyKaon());    // seperater Pion/Kaon/Proton
	    pid->calculate();
		if( !( pid->IsPidInfoValid() ) )
        {
        	m_prob_pid_k  = -999 ;
	        m_prob_pid_p  = -999 ;
	        m_prob_pid_pi = -999 ; 
        }
        else
        {
		    m_prob_pid_k  = pid->probKaon();
	        m_prob_pid_p  = pid->probProton();
	        m_prob_pid_pi = pid->probPion();
        }
    	if(m_prob_pid_p>0 && m_prob_pid_p>m_prob_pid_k && m_prob_pid_p>m_prob_pid_pi){

			mdcKalTrk->setPidType(RecMdcKalTrack::proton);
			
			int charge = mdcKalTrk->charge();

			HepLorentzVector proton_temp;
			proton_temp.setPx(mdcKalTrk->px());
			proton_temp.setPy(mdcKalTrk->py());
			proton_temp.setPz(mdcKalTrk->pz());
			double p3 = proton_temp.mag();
			proton_temp.setE(sqrt(p3*p3+mp*mp));
			
			if (charge==1){
		        ProtonP.push_back(proton_temp);
				ProtonP_index.push_back(i);
				nProtonP++;
			}else if(charge==-1){
		 		ProtonM.push_back(proton_temp);
				ProtonM_index.push_back(i);
				nProtonM++;
			}
		
		}
 
		RecMdcKalTrack* mdcKalTrk2 = (*itTrk)->mdcKalTrack();

		ParticleID * pid2=ParticleID::instance();
		double m_prob_pid2_pi,m_prob_pid2_k,m_prob_pid2_p;
	    pid2->init();
	    pid2->setMethod(pid2->methodProbability());
	    pid2->setChiMinCut(4);
	    pid2->setRecTrack(*itTrk);
	    pid2->usePidSys(pid2->useDedx() | pid2->useTof1() | pid2->useTof2() | pid2->useTof()); // use PID sub-system
	    pid2->identify(pid2->onlyPion() | pid2->onlyKaon());    // seperater Pion/Kaon/Proton
	    pid2->calculate();
		if( !( pid2->IsPidInfoValid() ) ) 
        {
            m_prob_pid2_k  = -999;
	        m_prob_pid2_pi = -999;
        }
        else
        {
            m_prob_pid2_k  = pid2->probKaon();
	        m_prob_pid2_pi = pid2->probPion();
        }
		   
        if( m_prob_pid2_pi>0 && m_prob_pid2_pi>m_prob_pid2_k){

			mdcKalTrk2->setPidType(RecMdcKalTrack::pion);

			int charge = mdcKalTrk2->charge();

			HepLorentzVector pion_temp;
			pion_temp.setPx(mdcKalTrk2->px());
			pion_temp.setPy(mdcKalTrk2->py());
			pion_temp.setPz(mdcKalTrk2->pz());
			double p3 = pion_temp.mag2();
			pion_temp.setE(sqrt(p3*p3+mpi*mpi));
			
			if (charge==1){
		        PionP.push_back(pion_temp);
				PionP_index.push_back(i);
				nPionP++;
			}else if(charge==-1){
		 		PionM.push_back(pion_temp);
				PionM_index.push_back(i);
				nPionM++;
			}

		}

	}

    return TrackNumber;

}


bool massXi::IsXi(SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,vector<HepLorentzVector> ProtonP,vector<HepLorentzVector> PionM,vector<int> ProtonP_index,vector<int> PionM_index,string flag,vector<WTrackParameter>& SelectedTracks){


	Hep3Vector ip0(0,0,0);
	HepSymMatrix ip0Ex(3,0);
	IVertexDbSvc*  vtxsvc;
	Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc); 
	if(vtxsvc->isVertexValid()){
		double* dbv = vtxsvc->PrimaryVertex();
		double* vv  = vtxsvc->SigmaPrimaryVertex();
		ip0.setX(dbv[0]);
		ip0.setY(dbv[1]);
		ip0.setZ(dbv[2]);
		ip0Ex[0][0] = vv[0] * vv[0];
		ip0Ex[1][1] = vv[1] * vv[1];
		ip0Ex[2][2] = vv[2] * vv[2];
	}else{
		 cout << "GetIP Failure" << endl;
		return false;
	}


	// * * * * * * * * * * * VertexFit and SecondVertexFit of Lambda and Xi * * * * * * * * * * * * *
	VertexFit       *vtxfit  = VertexFit::instance();
	SecondVertexFit *svtxfit = SecondVertexFit::instance();

	// Initialized Before VertexFit, Used For VertexFit
	WTrackParameter wtrkproton;
	WTrackParameter wtrkpi1;
	WTrackParameter wtrkpi2;

	// Assigned After VertexFit, WTrackParameter Renewed by VertexFit
	WTrackParameter wXi;
	WTrackParameter wlambda;

	// Interaction Point v0, v1, v2
	VertexParameter v0par;
	VertexParameter v1par;
	VertexParameter v2par;

	double Delta  = 2.0;

	HepPoint3D    vx(0., 0., 0.);
	HepSymMatrix  Evx(3, 0);
	double bx = 1E+6;
	double by = 1E+6;
	double bz = 1E+6;
	Evx[0][0] = bx * bx;
	Evx[1][1] = by * by;
	Evx[2][2] = bz * bz;
	VertexParameter vxpar_lambda;
	vxpar_lambda.setVx(vx);
	vxpar_lambda.setEvx(Evx);
	VertexParameter vxpar_Xi;
	vxpar_Xi.setVx(vx);
	vxpar_Xi.setEvx(Evx);


	HepLorentzVector selectedXi_recoil1C;

	HepLorentzVector selectedXi;
	HepLorentzVector selectedLbd;

	HepVector LbdVertex;
	HepVector XiVertex;

    int pionLbdIndex;
    int pionXiIndex;
    int protonLbdIndex;

    HepLorentzVector Proton4Lbd;
	HepLorentzVector Pion4Lbd;
	HepLorentzVector Pion4Xi;

    double mXitemp;
	double mXiRecoiltemp;
	double mLbdtemp;
	double DE_Xitemp;

    double X2_vf1_Lbdtemp;
	double X2_vf2_Lbdtemp;
	double X2_vf1_Xitemp;
	double X2_vf2_Xitemp;
	
	double decayLength_Lbdtemp;
	double decayError_Lbdtemp;
	double decayLength_Xitemp;
	double decayError_Xitemp;


	double best_mass = 0;
    bool pass = false;

	for(int i = 0; i < ProtonP_index.size(); i++){
		RecMdcKalTrack *protonTrk = (*(evtRecTrkCol->begin()+ProtonP_index[i]))->mdcKalTrack();
		protonTrk->setPidType(RecMdcKalTrack::proton);
		wtrkproton = WTrackParameter(mp, protonTrk->getZHelixP(), protonTrk->getZErrorP());

		for(int j = 0; j < PionM_index.size(); j++){
			RecMdcKalTrack *pi1Trk = (*(evtRecTrkCol->begin()+PionM_index[j]))->mdcKalTrack();
			pi1Trk->setPidType(RecMdcKalTrack::pion);
			wtrkpi1 = WTrackParameter(mpi, pi1Trk->getZHelix(), pi1Trk->getZError());

            // primary vertex fitting for Lbd -> P Pi
			vtxfit->init();
			vtxfit->AddTrack(0, wtrkproton);
			vtxfit->AddTrack(1, wtrkpi1);
			vtxfit->AddVertex(0, vxpar_lambda, 0, 1);
			if(!vtxfit->Fit(0)) continue;

			vtxfit->Swim(0);
			vtxfit->BuildVirtualParticle(0);
			wlambda = vtxfit->wVirtualTrack(0);
			v2par   = vtxfit->vpar(0);
			double chi2_VFL_temp = vtxfit->chisq(0);
            if( chi2_VFL_temp > 20 ) continue;

			HepVector LbdVertex_temp = vtxfit->Vx(0);
            HepLorentzVector Proton4Lbd_temp = vtxfit->pfit(0);
            HepLorentzVector Pion4Lbd_temp = vtxfit->pfit(1);
	        HepLorentzVector Plambda4cut =  Proton4Lbd_temp + Pion4Lbd_temp ;	
            double massLbd4cut = Plambda4cut.m();
		    if( !( 1.111<massLbd4cut && massLbd4cut<1.121 ) ) continue;

			for(int k = 0; k < PionM_index.size(); k++){
				if(j == k) continue;

				RecMdcKalTrack *pi2Trk = (*(evtRecTrkCol->begin()+PionM_index[k]))->mdcKalTrack();
				pi2Trk->setPidType(RecMdcKalTrack::pion);
				wtrkpi2 = WTrackParameter(mpi, pi2Trk->getZHelix(), pi2Trk->getZError());

				//primary vertex for lambda+pi=Xi
				vtxfit->init();
				vtxfit->AddTrack(0, wlambda);
				vtxfit->AddTrack(1, wtrkpi2);
				vtxfit->AddVertex(0, vxpar_Xi, 0, 1);
				if(!vtxfit->Fit(0)) continue;
				
				vtxfit->Swim(0);
				vtxfit->BuildVirtualParticle(0);
				wXi   = vtxfit->wVirtualTrack(0);
				v1par = vtxfit->vpar(0);

			    HepLorentzVector P4_lambda_temp = vtxfit->pfit(0);
				HepLorentzVector P4_Xi_temp = vtxfit->pfit(0) + vtxfit->pfit(1);
                HepLorentzVector Pion4Xi_temp = vtxfit->pfit(1);
				
                HepVector XiVertex_temp = vtxfit->Vx(0);
				double chi2_VFX_temp = vtxfit->chisq(0);
                if( chi2_VFX_temp > 20 ) continue;

                // secondary vertex fit for Lambda
                svtxfit->init();
				svtxfit->setPrimaryVertex(v1par);
				svtxfit->AddTrack(0, wlambda);
				svtxfit->setVpar(v2par);

				if(!svtxfit->Fit()) continue;
				double chi2_SVFL_temp  = svtxfit->chisq();
				double len_lambda_temp = svtxfit->decayLength();
				double error_lambda_temp = svtxfit->decayLengthError();
                if( chi2_SVFL_temp > 20 ) continue;

				//secondary vertex fir for Xi
				v0par.setVx(ip0);
				v0par.setEvx(ip0Ex);
				svtxfit->init();
				svtxfit->setPrimaryVertex(v0par);
				svtxfit->AddTrack(0, wXi);
				svtxfit->setVpar(v1par);
				if(!svtxfit->Fit()) continue;
				double chi2_SVFX_temp = svtxfit->chisq();
				double len_Xi_temp    = svtxfit->decayLength();
                double error_Xi_temp  = svtxfit->decayLengthError();
                if(  chi2_SVFX_temp > 20 ) continue;

				//next code is used to calculate the recoil mass
                HepLorentzVector p4_lab(0.034,0,0,3.097);			
                HepLorentzVector recoil_p4_lab = p4_lab - P4_Xi_temp; 
				double recoil_mass = recoil_p4_lab.m(); 

                // kill the multiple candidates
				if(abs(recoil_mass-1.32132)<abs(best_mass-1.32132)){
    
                    pass = true;
					best_mass = recoil_mass;

                    SelectedTracks.clear();
			        SelectedTracks.push_back(wtrkproton);
			        SelectedTracks.push_back(wtrkpi1);
			        SelectedTracks.push_back(wtrkpi2);

					LbdVertex = LbdVertex_temp;
					XiVertex = XiVertex_temp;

                    pionLbdIndex = PionM_index[j]; 
                    pionXiIndex = PionM_index[k]; 
                    protonLbdIndex = ProtonP_index[i];

                    X2_vf2_Lbdtemp = chi2_SVFL_temp;
					X2_vf2_Xitemp = chi2_SVFX_temp;
				
					X2_vf1_Lbdtemp = chi2_VFL_temp;
					X2_vf1_Xitemp = chi2_VFX_temp;

					decayLength_Lbdtemp = len_lambda_temp;
					decayError_Lbdtemp = error_lambda_temp;
					decayLength_Xitemp = len_Xi_temp;
					decayError_Xitemp = error_Xi_temp;

				}
			}
		}
	}

	if(pass&&flag=="Xibar"){

         vector<int> trackIndex;
         trackIndex.push_back(protonLbdIndex);
         trackIndex.push_back(pionLbdIndex);
         trackIndex.push_back(pionXiIndex);

         vector<int> trackIndexTruth;
         trackIndexTruth.push_back(2212);
         trackIndexTruth.push_back(211);
         trackIndexTruth.push_back(211);

         if(runNumber<0&&angleMatch_switch == "1"){
             if(angleMatch(-3312,trackIndex,trackIndexTruth)){
                 XibarMatch = 1;
             }else{
                 XibarMatch = 0;
             }
         }else{
             XibarMatch = -1;
         }
         
         XibarVertexX = XiVertex[0];
		 XibarVertexY = XiVertex[1];
		 XibarVertexZ = XiVertex[2];
		 XibarVertexR = XiVertex.norm();

		 LbdbarVertexX = LbdVertex[0];
		 LbdbarVertexY = LbdVertex[1];
		 LbdbarVertexZ = LbdVertex[2];
		 LbdbarVertexR = LbdVertex.norm();
		
         X2_vf2_Lbdbar = X2_vf2_Lbdtemp;
		 X2_vf2_Xibar = X2_vf2_Xitemp;
		 
		 X2_vf1_Lbdbar = X2_vf1_Lbdtemp;
		 X2_vf1_Xibar = X2_vf1_Xitemp;
		 
		 decayLength_Lbdbar = decayLength_Lbdtemp;
		 decayError_Lbdbar = decayError_Lbdtemp;
		 decayLength_Xibar = decayLength_Xitemp;
		 decayError_Xibar = decayError_Xitemp;

         return true;
	}

	if(pass&&flag=="Xi"){

         vector<int> trackIndex;
         trackIndex.push_back(protonLbdIndex);
         trackIndex.push_back(pionLbdIndex);
         trackIndex.push_back(pionXiIndex);

         vector<int> trackIndexTruth;
         trackIndexTruth.push_back(2212);
         trackIndexTruth.push_back(211);
         trackIndexTruth.push_back(211);

         if(runNumber<0&&angleMatch_switch == "1"){
            if(angleMatch(3312,trackIndex,trackIndexTruth)){
                XiMatch = 1;
            }else{
                XiMatch = 0;
            }
         }else{
            XiMatch = -1;
         }

         XiVertexX = XiVertex[0];
		 XiVertexY = XiVertex[1];
		 XiVertexZ = XiVertex[2];
		 XiVertexR = XiVertex.norm();
			
		 LbdVertexX = LbdVertex[0];
		 LbdVertexY = LbdVertex[1];
		 LbdVertexZ = LbdVertex[2];
		 LbdVertexR = LbdVertex.norm();
				
		 X2_vf1_Lbd = X2_vf1_Lbdtemp;
		 X2_vf1_Xi = X2_vf1_Xitemp;

		 X2_vf2_Lbd = X2_vf2_Lbdtemp;
		 X2_vf2_Xi = X2_vf2_Xitemp;

		 decayLength_Lbd = decayLength_Lbdtemp;
		 decayError_Lbd = decayError_Lbdtemp;
		 decayLength_Xi = decayLength_Xitemp;
		 decayError_Xi = decayError_Xitemp;

        return true;
	}

    return false;
}



