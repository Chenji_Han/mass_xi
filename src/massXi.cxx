/************************************************************************
  > File Name: massXi.cxx
  > Author: Chenji Han
  > Mail: hanchenji16@mails.ucas.ac.cn 
 ************************************************************************/


#include "massXiAlg/massXi.h" // header file

using Event::McParticle;
using Event::McParticleCol;

int ievt = 0;
//int N1=0;
//int N2=0;
//int N3=0;


massXi::massXi(const std::string& name, ISvcLocator* pSvcLocator):Algorithm(name, pSvcLocator) {
	//	do nothing  
}

StatusCode massXi::initialize(){

	MsgStream log(msgSvc(), name());
	log << MSG::INFO << "in initialize()" << endmsg;

    // next few lines of code are used to read some parameters fromt the inputs file
    string outputName;
    string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
            if (!line.length() || line[0] == '#')
                continue;
    
    	std::istringstream iss(line);
            string a,b,c;
            iss>>a>>b>>c;
            if(a=="dst_outputName"){
    	        outputName = b;
    	    }else if(a=="dst_angleMatch"){
                angleMatch_switch = b;
            }
    
    }

	//next codes are used to initialize the match service
	IMCTruthMatchSvc *i_matchSvc;
	StatusCode sc_MC = service("MCTruthMatchSvc",i_matchSvc);
	if(sc_MC.isFailure()){
		cout<<"MCTruthMatchSvc load fail"<<endl;
		return 0;
	}
	matchSvc=dynamic_cast<MCTruthMatchSvc*>(i_matchSvc);

    outputFile = new TFile(outputName.c_str(),"recreate");
    
    outputTreeXi = new TTree("Xi","");
    outputTreeXi_truth = new TTree("XiTruth","");
    outputTreeXibar = new TTree("Xibar","");
    outputTreeXibar_truth = new TTree("XibarTruth","");

	outputTreeXibar_truth -> Branch("indexmc",&m_numParticle);
	outputTreeXibar_truth -> Branch("pdgid",m_pdgid,"pdgid[100]/I");
	outputTreeXibar_truth -> Branch("motheridx",m_motheridx,"motheridx[100]/I");
	outputTreeXibar_truth -> Branch("runNumber",&runNumber);
    outputTreeXibar_truth -> Branch("eventNumber",&eventNumber);

    outputTreeXibar -> Branch("indexmc",&m_numParticle);
	outputTreeXibar -> Branch("pdgid",m_pdgid,"pdgid[100]/I");
	outputTreeXibar -> Branch("motheridx",m_motheridx,"motheridx[100]/I");
	outputTreeXibar -> Branch("runNumber",&runNumber);
    outputTreeXibar -> Branch("eventNumber",&eventNumber);
    outputTreeXibar -> Branch("Ntrack",&Ntrack);
    outputTreeXibar -> Branch("NProtonP",&NProtonP);
    outputTreeXibar -> Branch("NProtonM",&NProtonM);
    outputTreeXibar -> Branch("NPionM",&NPionM);
    outputTreeXibar -> Branch("NPionP",&NPionP);

	outputTreeXi_truth -> Branch("indexmc",&m_numParticle);
	outputTreeXi_truth -> Branch("pdgid",m_pdgid,"pdgid[100]/I");
	outputTreeXi_truth -> Branch("motheridx",m_motheridx,"motheridx[100]/I");
	outputTreeXi_truth -> Branch("runNumber",&runNumber);
    outputTreeXi_truth -> Branch("eventNumber",&eventNumber);

    outputTreeXi -> Branch("indexmc",&m_numParticle);
	outputTreeXi -> Branch("pdgid",m_pdgid,"pdgid[100]/I");
	outputTreeXi -> Branch("motheridx",m_motheridx,"motheridx[100]/I");
	outputTreeXi -> Branch("runNumber",&runNumber);
    outputTreeXi -> Branch("eventNumber",&eventNumber);
    outputTreeXi -> Branch("Ntrack",&Ntrack);
    outputTreeXi -> Branch("NProtonP",&NProtonP);
    outputTreeXi -> Branch("NProtonM",&NProtonM);
    outputTreeXi -> Branch("NPionM",&NPionM);
    outputTreeXi -> Branch("NPionP",&NPionP);

   //////////////////////////////////////////////////////////////
   
	outputTreeXi_truth -> Branch("mXip4",&mXip4_truth);
	outputTreeXi_truth -> Branch("mXi",&mXi_truth);
    outputTreeXi_truth -> Branch("mLbd",&mLbd_truth);
    outputTreeXi_truth -> Branch("DE_Xi",&DE_Xi_truth);
    
	outputTreeXi_truth -> Branch("decayLength_Lbd",&decayLength_Lbd_truth);
	outputTreeXi_truth -> Branch("decayLength_Xi",&decayLength_Xi_truth);
	
	outputTreeXi_truth -> Branch("ProtonPPT",&ProtonPPT_truth);
	outputTreeXi_truth -> Branch("ProtonPPX",&ProtonPPX_truth);
	outputTreeXi_truth -> Branch("ProtonPPY",&ProtonPPY_truth);
	outputTreeXi_truth -> Branch("ProtonPPZ",&ProtonPPZ_truth);
	outputTreeXi_truth -> Branch("PionMXiPT",&PionMXiPT_truth);
	outputTreeXi_truth -> Branch("PionMXiPX",&PionMXiPX_truth);
	outputTreeXi_truth -> Branch("PionMXiPY",&PionMXiPY_truth);
	outputTreeXi_truth -> Branch("PionMXiPZ",&PionMXiPZ_truth);
	outputTreeXi_truth -> Branch("PionMLbdPT",&PionMLbdPT_truth);
	outputTreeXi_truth -> Branch("PionMLbdPX",&PionMLbdPX_truth);
	outputTreeXi_truth -> Branch("PionMLbdPY",&PionMLbdPY_truth);
	outputTreeXi_truth -> Branch("PionMLbdPZ",&PionMLbdPZ_truth);
	outputTreeXi_truth -> Branch("XiPT",&XiPT_truth);
	outputTreeXi_truth -> Branch("XiPX",&XiPX_truth);
	outputTreeXi_truth -> Branch("XiPY",&XiPY_truth);
	outputTreeXi_truth -> Branch("XiPZ",&XiPZ_truth);
	outputTreeXi_truth -> Branch("LbdPT",&LbdPT_truth);
	outputTreeXi_truth -> Branch("LbdPX",&LbdPX_truth);
	outputTreeXi_truth -> Branch("LbdPY",&LbdPY_truth);
	outputTreeXi_truth -> Branch("LbdPZ",&LbdPZ_truth);


	outputTreeXi_truth -> Branch("XiVertexX",&XiVertexX_truth);
	outputTreeXi_truth -> Branch("XiVertexY",&XiVertexY_truth);
	outputTreeXi_truth -> Branch("XiVertexZ",&XiVertexZ_truth);
	outputTreeXi_truth -> Branch("XiVertexR",&XiVertexR_truth);
	outputTreeXi_truth -> Branch("LbdVertexX",&LbdVertexX_truth);
	outputTreeXi_truth -> Branch("LbdVertexY",&LbdVertexY_truth);
	outputTreeXi_truth -> Branch("LbdVertexZ",&LbdVertexZ_truth);
	outputTreeXi_truth -> Branch("LbdVertexR",&LbdVertexR_truth);

	///////////////////////////////////////////////////

    outputTreeXi -> Branch("XiMatch",&XiMatch);
	
    outputTreeXi -> Branch("chisq5C",&chisq5C);
    outputTreeXi -> Branch("mXip4",&mXip4);
	outputTreeXi -> Branch("mXi",&mXi);
    outputTreeXi -> Branch("mXiRecoil",&mXiRecoil);
    outputTreeXi -> Branch("mLbd",&mLbd);
    outputTreeXi -> Branch("DE_Xi",&DE_Xi);
    
	outputTreeXi -> Branch("X2_vf1_Lbd",&X2_vf1_Lbd);
	outputTreeXi -> Branch("X2_vf2_Lbd",&X2_vf2_Lbd);
	outputTreeXi -> Branch("X2_vf1_Xi",&X2_vf1_Xi);
	outputTreeXi -> Branch("X2_vf2_Xi",&X2_vf2_Xi);

	outputTreeXi -> Branch("decayLength_Lbd",&decayLength_Lbd);
	outputTreeXi -> Branch("decayError_Lbd",&decayError_Lbd);
	outputTreeXi -> Branch("decayLength_Xi",&decayLength_Xi);
	outputTreeXi -> Branch("decayError_Xi",&decayError_Xi);
		
	outputTreeXi -> Branch("ProtonPPT",&ProtonPPT);
	outputTreeXi -> Branch("ProtonPPX",&ProtonPPX);
	outputTreeXi -> Branch("ProtonPPY",&ProtonPPY);
	outputTreeXi -> Branch("ProtonPPZ",&ProtonPPZ);
	outputTreeXi -> Branch("PionMXiPT",&PionMXiPT);
	outputTreeXi -> Branch("PionMXiPX",&PionMXiPX);
	outputTreeXi -> Branch("PionMXiPY",&PionMXiPY);
	outputTreeXi -> Branch("PionMXiPZ",&PionMXiPZ);
	outputTreeXi -> Branch("PionMLbdPT",&PionMLbdPT);
	outputTreeXi -> Branch("PionMLbdPX",&PionMLbdPX);
	outputTreeXi -> Branch("PionMLbdPY",&PionMLbdPY);
	outputTreeXi -> Branch("PionMLbdPZ",&PionMLbdPZ);
	outputTreeXi -> Branch("XiPT",&XiPT);
	outputTreeXi -> Branch("XiPX",&XiPX);
	outputTreeXi -> Branch("XiPY",&XiPY);
	outputTreeXi -> Branch("XiPZ",&XiPZ);
	outputTreeXi -> Branch("LbdPT",&LbdPT);
	outputTreeXi -> Branch("LbdPX",&LbdPX);
	outputTreeXi -> Branch("LbdPY",&LbdPY);
	outputTreeXi -> Branch("LbdPZ",&LbdPZ);

	outputTreeXi -> Branch("XiVertexX",&XiVertexX);
	outputTreeXi -> Branch("XiVertexY",&XiVertexY);
	outputTreeXi -> Branch("XiVertexZ",&XiVertexZ);
	outputTreeXi -> Branch("XiVertexR",&XiVertexR);
	outputTreeXi -> Branch("LbdVertexX",&LbdVertexX);
	outputTreeXi -> Branch("LbdVertexY",&LbdVertexY);
	outputTreeXi -> Branch("LbdVertexZ",&LbdVertexZ);
	outputTreeXi -> Branch("LbdVertexR",&LbdVertexR);


	////////////////////////////////////////////////////////////
		
	outputTreeXibar -> Branch("XibarMatch",&XibarMatch);

    outputTreeXibar -> Branch("chisq5C",&chisq5C);
	outputTreeXibar -> Branch("mXibarp4",&mXibarp4);
	outputTreeXibar -> Branch("mXibar",&mXibar);
    outputTreeXibar -> Branch("mXibarRecoil",&mXibarRecoil);
    outputTreeXibar -> Branch("mLbdbar_pre1C",&mLbdbar);
    outputTreeXibar -> Branch("DE_Xibar",&DE_Xibar);
     
	outputTreeXibar -> Branch("X2_vf1_Lbdbar",&X2_vf1_Lbdbar);
	outputTreeXibar -> Branch("X2_vf2_Lbdbar",&X2_vf2_Lbdbar);
	outputTreeXibar -> Branch("X2_vf1_Xibar",&X2_vf1_Xibar);
	outputTreeXibar -> Branch("X2_vf2_Xibar",&X2_vf2_Xibar);

	outputTreeXibar -> Branch("decayLength_Lbdbar",&decayLength_Lbdbar);
	outputTreeXibar -> Branch("decayError_Lbdbar",&decayError_Lbdbar);
	outputTreeXibar -> Branch("decayLength_Xibar",&decayLength_Xibar);
	outputTreeXibar -> Branch("decayError_Xibar",&decayError_Xibar);

	outputTreeXibar -> Branch("ProtonMPT",&ProtonMPT);
	outputTreeXibar -> Branch("ProtonMPX",&ProtonMPX);
	outputTreeXibar -> Branch("ProtonMPY",&ProtonMPY);
	outputTreeXibar -> Branch("ProtonMPZ",&ProtonMPZ);
	outputTreeXibar -> Branch("PionPXibarPT",&PionPXiPT);
	outputTreeXibar -> Branch("PionPXibarPX",&PionPXiPX);
	outputTreeXibar -> Branch("PionPXibarPY",&PionPXiPY);
	outputTreeXibar -> Branch("PionPXibarPZ",&PionPXiPZ);
	outputTreeXibar -> Branch("PionPLbdbarPT",&PionPLbdPT);
	outputTreeXibar -> Branch("PionPLbdbarPX",&PionPLbdPX);
	outputTreeXibar -> Branch("PionPLbdbarPY",&PionPLbdPY);
	outputTreeXibar -> Branch("PionPLbdbarPZ",&PionPLbdPZ);
	outputTreeXibar -> Branch("XibarPT",&XibarPT);
	outputTreeXibar -> Branch("XibarPX",&XibarPX);
	outputTreeXibar -> Branch("XibarPY",&XibarPY);
	outputTreeXibar -> Branch("XibarPZ",&XibarPZ);
	outputTreeXibar -> Branch("LbdbarPT",&LbdbarPT);
	outputTreeXibar -> Branch("LbdbarPX",&LbdbarPX);
	outputTreeXibar -> Branch("LbdbarPX",&LbdbarPY);
	outputTreeXibar -> Branch("LbdbarPZ",&LbdbarPZ);

	outputTreeXibar -> Branch("XibarVertexX",&XibarVertexX);
	outputTreeXibar -> Branch("XibarVertexY",&XibarVertexY);
	outputTreeXibar -> Branch("XibarVertexZ",&XibarVertexZ);
	outputTreeXibar -> Branch("XibarVertexR",&XibarVertexR);
	outputTreeXibar -> Branch("LbdbarVertexX",&LbdbarVertexX);
	outputTreeXibar -> Branch("LbdbarVertexY",&LbdbarVertexY);
	outputTreeXibar -> Branch("LbdbarVertexZ",&LbdbarVertexZ);
	outputTreeXibar -> Branch("LbdbarVertexR",&LbdbarVertexR);

	//////////////////////////////////////////////////
	
	outputTreeXibar_truth -> Branch("mXibarp4",&mXibarp4_truth);
	outputTreeXibar_truth -> Branch("mXibar",&mXibar_truth);
    outputTreeXibar_truth -> Branch("mLbdbar",&mLbdbar_truth);
    outputTreeXibar_truth -> Branch("DE_Xibar",&DE_Xibar_truth);
    
	outputTreeXibar_truth -> Branch("decayLength_Lbdbar",&decayLength_Lbdbar_truth);
	outputTreeXibar_truth -> Branch("decayLength_Xibar",&decayLength_Xibar_truth);

	outputTreeXibar_truth -> Branch("ProtonMPT",&ProtonMPT_truth);
	outputTreeXibar_truth -> Branch("ProtonMPX",&ProtonMPX_truth);
	outputTreeXibar_truth -> Branch("ProtonMPY",&ProtonMPY_truth);
	outputTreeXibar_truth -> Branch("ProtonMPZ",&ProtonMPZ_truth);
	outputTreeXibar_truth -> Branch("PionPXibarPT",&PionPXiPT_truth);
	outputTreeXibar_truth -> Branch("PionPXibarPX",&PionPXiPX_truth);
	outputTreeXibar_truth -> Branch("PionPXibarPY",&PionPXiPY_truth);
	outputTreeXibar_truth -> Branch("PionPXibarPZ",&PionPXiPZ_truth);
	outputTreeXibar_truth -> Branch("PionPLbdbarPT",&PionPLbdPT_truth);
	outputTreeXibar_truth -> Branch("PionPLbdbarPX",&PionPLbdPX_truth);
	outputTreeXibar_truth -> Branch("PionPLbdbarPY",&PionPLbdPY_truth);
	outputTreeXibar_truth -> Branch("PionPLbdbarPZ",&PionPLbdPZ_truth);
	outputTreeXibar_truth -> Branch("XibarPT",&XibarPT_truth);
	outputTreeXibar_truth -> Branch("XibarPX",&XibarPX_truth);
	outputTreeXibar_truth -> Branch("XibarPY",&XibarPY_truth);
	outputTreeXibar_truth -> Branch("XibarPZ",&XibarPZ_truth);
	outputTreeXibar_truth -> Branch("LbdbarPT",&LbdbarPT_truth);
	outputTreeXibar_truth -> Branch("LbdbarPX",&LbdbarPX_truth);
	outputTreeXibar_truth -> Branch("LbdbarPX",&LbdbarPY_truth);
	outputTreeXibar_truth -> Branch("LbdbarPZ",&LbdbarPZ_truth);

	outputTreeXibar_truth -> Branch("XibarVertexX",&XibarVertexX_truth);
	outputTreeXibar_truth -> Branch("XibarVertexY",&XibarVertexY_truth);
	outputTreeXibar_truth -> Branch("XibarVertexZ",&XibarVertexZ_truth);
	outputTreeXibar_truth -> Branch("XibarVertexR",&XibarVertexR_truth);
	outputTreeXibar_truth -> Branch("LbdbarVertexX",&LbdbarVertexX_truth);
	outputTreeXibar_truth -> Branch("LbdbarVertexY",&LbdbarVertexY_truth);
	outputTreeXibar_truth -> Branch("LbdbarVertexZ",&LbdbarVertexZ_truth);
	outputTreeXibar_truth -> Branch("LbdbarVertexR",&LbdbarVertexR_truth);


	log << MSG::INFO << "successfully return from initialize()" <<endmsg;
	return StatusCode::SUCCESS;
}


StatusCode massXi::execute(){

    ievt++;
	//ievt++;	if(ievt%500==0) cout<<"processing..."<<ievt<<endl;

	SmartDataPtr<Event::McParticleCol> mcParticleCol(eventSvc(),"/Event/MC/McParticleCol");
	SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
	SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), EventModel::EvtRec::EvtRecEvent);
	SmartDataPtr<EvtRecTrackCol> evtRecTrkCol(eventSvc(),  EventModel::EvtRec::EvtRecTrackCol);

	runNumber=eventHeader->runNumber();
	eventNumber=eventHeader->eventNumber();

    if( runNumber<0 ){
	    MCtopo(mcParticleCol);
	    MCtruth(mcParticleCol);
    }

	vector<HepLorentzVector> PionPinfo; PionPinfo.clear();
	vector<HepLorentzVector> PionMinfo; PionMinfo.clear();
	vector<HepLorentzVector> ProtonMinfo; ProtonMinfo.clear();
	vector<HepLorentzVector> ProtonPinfo; ProtonPinfo.clear();

	vector<int> PionP_index; PionP_index.clear();
	vector<int> PionM_index; PionM_index.clear();
	vector<int> ProtonP_index; ProtonP_index.clear();
	vector<int> ProtonM_index; ProtonM_index.clear();

	Ntrack = IsGoodTrack(evtRecEvent,evtRecTrkCol,PionPinfo,PionMinfo,ProtonPinfo,ProtonMinfo,
										 PionP_index,PionM_index,ProtonP_index,ProtonM_index,
                                         NProtonP,NProtonM,NPionP,NPionM);
    //if( Ntrack>=3 ) N1++;
    //if( Ntrack>=3 && NProtonP>=1 && NPionM>=2 ) N2++;

    // next is to reconstruct Xi
    vector<WTrackParameter> XiSelectedTracks; XiSelectedTracks.clear();
    bool xireco = IsXi(evtRecTrkCol,ProtonPinfo,PionMinfo,ProtonP_index,PionM_index,"Xi",XiSelectedTracks);
    vector<WTrackParameter> XibarSelectedTracks; XibarSelectedTracks.clear();
	bool xibarreco = IsXi(evtRecTrkCol,ProtonMinfo,PionPinfo,ProtonM_index,PionP_index,"Xibar",XibarSelectedTracks);

    if( !(xireco&&xibarreco) ){
	    return StatusCode::SUCCESS;
    }

    //cout<<"pass1"<<endl;

    HepLorentzVector p4_lab(0.034,0,0,3.097);			
   
    KalmanKinematicFit * kfit_ = KalmanKinematicFit::instance();
	kfit_->init();
	kfit_->AddTrack(0, XiSelectedTracks[0]);
	kfit_->AddTrack(1, XiSelectedTracks[1]);
	kfit_->AddTrack(2, XiSelectedTracks[2]);
	kfit_->AddTrack(3, XibarSelectedTracks[0]);
	kfit_->AddTrack(4, XibarSelectedTracks[1]);
	kfit_->AddTrack(5, XibarSelectedTracks[2]);

    vector<int> tlis1; tlis1.push_back(0); tlis1.push_back(1); tlis1.push_back(2);
    vector<int> tlis2; tlis2.push_back(3); tlis2.push_back(4); tlis2.push_back(5);

	kfit_->AddFourMomentum(0, p4_lab);
    kfit_->AddEqualMass(1,tlis1,tlis2);
    kfit_->AddResonance(2,1.115683,0,1);
	kfit_->AddResonance(3,1.115683,3,4);
    
    if( !(kfit_->Fit()) ){
	    return StatusCode::SUCCESS;
    }
    
    chisq5C = kfit_ -> chisq();
    //cout<<"pass2"<<endl;

    HepLorentzVector ProtonP4Lbd = kfit_ -> pfit(0);
    HepLorentzVector PionM4Lbd   = kfit_ -> pfit(1);
    HepLorentzVector PionM4Xi    = kfit_ -> pfit(2);
    HepLorentzVector selectedXi  = kfit_ -> pfit(0) + kfit_ -> pfit(1) + kfit_ -> pfit(2); 
    HepLorentzVector selectedLbd = kfit_ -> pfit(0) + kfit_ -> pfit(1); 

    HepLorentzVector ProtonM4Lbd = kfit_ -> pfit(3);
    HepLorentzVector PionP4Lbd   = kfit_ -> pfit(4);
    HepLorentzVector PionP4Xi    = kfit_ -> pfit(5);
    HepLorentzVector selectedXibar  = kfit_ -> pfit(3) + kfit_ -> pfit(4) + kfit_ -> pfit(5); 
    HepLorentzVector selectedLbdbar = kfit_ -> pfit(3) + kfit_ -> pfit(4); 

    mXip4 = selectedXi.m();
	mXi = sqrt( (0.5*3.097)*(0.5*3.097) - selectedXi.v().mag2() );
    mXiRecoil = (p4_lab - selectedXi).m();
    mLbd = selectedLbd.m();
    DE_Xi = 0.5*3.097 - selectedXi.e();

    mXibarp4 = selectedXibar.m();
	mXibar = sqrt( (0.5*3.097)*(0.5*3.097) - selectedXibar.v().mag2() );
    mXibarRecoil = (p4_lab - selectedXibar).m();
    mLbdbar = selectedLbdbar.m();
    DE_Xibar = 0.5*3.097 - selectedXibar.e();

    ProtonPPT = sqrt(ProtonP4Lbd.px()*ProtonP4Lbd.px()+ProtonP4Lbd.py()*ProtonP4Lbd.py());
	ProtonPPX = ProtonP4Lbd.px();
	ProtonPPY = ProtonP4Lbd.py();
	ProtonPPZ = ProtonP4Lbd.pz();

	PionMLbdPT = sqrt(PionM4Lbd.px()*PionM4Lbd.px()+PionM4Lbd.py()*PionM4Lbd.py());
	PionMLbdPX = PionM4Lbd.px();
	PionMLbdPY = PionM4Lbd.py();
	PionMLbdPZ = PionM4Lbd.pz();

	PionMXiPT = sqrt(PionM4Xi.px()*PionM4Xi.px()+PionM4Xi.py()*PionM4Xi.py());
	PionMXiPX = PionM4Xi.px();
	PionMXiPY = PionM4Xi.py();
	PionMXiPZ = PionM4Xi.pz();
	
    XiPT = sqrt(selectedXi.px()*selectedXi.px()+selectedXi.py()*selectedXi.py());
	XiPX = selectedXi.px();
	XiPY = selectedXi.py();
	XiPZ = selectedXi.pz();

	LbdPT = sqrt(selectedLbd.px()*selectedLbd.px()+selectedLbd.py()*selectedLbd.py());
	LbdPX = selectedLbd.px();
	LbdPY = selectedLbd.py();
	LbdPZ = selectedLbd.pz();



    ProtonMPT = sqrt(ProtonM4Lbd.px()*ProtonM4Lbd.px()+ProtonM4Lbd.py()*ProtonM4Lbd.py());
	ProtonMPX = ProtonM4Lbd.px();
	ProtonMPY = ProtonM4Lbd.py();
	ProtonMPZ = ProtonM4Lbd.pz();

	PionPLbdPT = sqrt(PionP4Lbd.px()*PionP4Lbd.px()+PionP4Lbd.py()*PionP4Lbd.py());
	PionPLbdPX = PionP4Lbd.px();
	PionPLbdPY = PionP4Lbd.py();
	PionPLbdPZ = PionP4Lbd.pz();

	PionPXiPT = sqrt(PionP4Xi.px()*PionP4Xi.px()+PionP4Xi.py()*PionP4Xi.py());
	PionPXiPX = PionP4Xi.px();
	PionPXiPY = PionP4Xi.py();
	PionPXiPZ = PionP4Xi.pz();

    XibarPT = sqrt(selectedXibar.px()*selectedXibar.px()+selectedXibar.py()*selectedXibar.py());
	XibarPX = selectedXibar.px();
	XibarPY = selectedXibar.py();
	XibarPZ = selectedXibar.pz();

	LbdbarPT = sqrt(selectedLbdbar.px()*selectedLbdbar.px()+selectedLbdbar.py()*selectedLbdbar.py());
	LbdbarPX = selectedLbdbar.px();
	LbdbarPY = selectedLbdbar.py();
	LbdbarPZ = selectedLbdbar.pz();


    outputTreeXibar_truth -> Fill();
    outputTreeXibar -> Fill();
    outputTreeXi_truth -> Fill();	
    outputTreeXi -> Fill();
   
	return StatusCode::SUCCESS;

}


StatusCode massXi::finalize() {

	outputFile -> cd();

	outputTreeXi -> Write();
	outputTreeXi_truth -> Write();
	outputTreeXibar -> Write();
	outputTreeXibar_truth -> Write();

	outputFile -> Close();

    //cout<<"cut flow:"<<endl;
    //cout<<"    all the events                         "<<ievt<<endl;
    //cout<<"    total charged tracks >= 3 :            "<<N1<<endl;
    //cout<<"    at least 2 pionms and 1 protonp :      "<<N2<<endl;
    //cout<<"    pass the vertex fit of Lambda and Xi : "<<N3<<endl;
    
	MsgStream log(msgSvc(), name());
	log << MSG::INFO << "in finalize()" << endmsg;
	return StatusCode::SUCCESS;
}











