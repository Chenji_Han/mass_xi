#include "GaudiKernel/DeclareFactoryEntries.h"
#include "massXiAlg/massXi.h"

DECLARE_ALGORITHM_FACTORY( massXi )

DECLARE_FACTORY_ENTRIES( massXiAlg ) {
  DECLARE_ALGORITHM(massXi);
}

