
# **Precise Measurement of Xi Mass**
This repository is used to do the cut flow of the Xi reconstruction  
There are four trees:

1. Xi
2. Xibar
3. XiTruth
4. XibarTruth

### Introduction to  Xi Xiar 4C

The total four-momentum fo Xi-Xibar pair is constrained to the beam four-momentum in order to improve
the resolution  

