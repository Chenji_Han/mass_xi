
#ifndef massXi_H
#define massXi_H 

#include<fstream>
#include"McTruth/McParticle.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/PropertyMgr.h"
#include "VertexFit/IVertexDbSvc.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"

#include "EventModel/EventModel.h"
#include "EventModel/Event.h"

#include "EvtRecEvent/EvtRecEvent.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "DstEvent/TofHitStatus.h"
#include "EventModel/EventHeader.h"

#include "VertexFit/Helix.h"
#include "VertexFit/SecondVertexFit.h"
#include "VertexFit/KalmanKinematicFit.h"

#include "TMath.h"
#include "GaudiKernel/INTupleSvc.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IHistogramSvc.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/TwoVector.h"

#include "VertexFit/KalmanKinematicFit.h"
#include "VertexFit/VertexFit.h"
#include "VertexFit/Helix.h"
#include "ParticleID/ParticleID.h"
#include <vector>

//#include "VertexFit/KinematicFit.h"
#include "VertexFit/KalmanKinematicFit.h"
#include "VertexFit/VertexFit.h"
#include "VertexFit/Helix.h"
#include "ParticleID/ParticleID.h"

#include "MCTruthMatchSvc/MCTruthMatchSvc.h"

#include "TLorentzVector.h"
#include <TFile.h>
#include <TTree.h>
//#include "Tracks_class.h"
//#include "TClonesArray.h"

using CLHEP::Hep3Vector;
using CLHEP::Hep2Vector;
using CLHEP::HepLorentzVector;
#include "CLHEP/Geometry/Point3D.h"
#ifndef ENABLE_BACKWARDS_COMPATIBILITY
   typedef HepGeom::Point3D<double> HepPoint3D;
#endif
typedef std::vector<int> Vint;
typedef std::vector<HepLorentzVector> Vp4;

const double mpion    = 0.13957;
const double mpi      = 0.13957;
const double mk       = 0.493677;
const double mp       = 0.938272;
const double mproton  = 0.938272;
const double mLbd     = 1.115683;


class massXi : public Algorithm {

	public:
	
	massXi(const std::string& name, ISvcLocator* pSvcLocator);
	
    StatusCode initialize();
  	
	StatusCode execute();

  	StatusCode finalize();  

    // in the next function, momentum and probability info of pions and protons are stored
	int IsGoodTrack(SmartDataPtr<EvtRecEvent>,SmartDataPtr<EvtRecTrackCol>,vector<HepLorentzVector>&,vector<HepLorentzVector>&,vector<HepLorentzVector>&,vector<HepLorentzVector>&,vector<int>&,vector<int>&,vector<int>&,vector<int>&,int&,int&,int&,int&);

    bool IsXi(SmartDataPtr<EvtRecTrackCol>,vector<HepLorentzVector>,vector<HepLorentzVector>,vector<int>,vector<int>,string,vector<WTrackParameter>&);

	void MCtopo(SmartDataPtr<Event::McParticleCol> mcParticleCol);
	void MCtruth(SmartDataPtr<Event::McParticleCol> mcParticleCol);

    bool angleMatch(int,vector<int>,vector<int>);
    
    double recoil1C ( WTrackParameter, double, HepLorentzVector& );

	private:

    string angleMatch_switch;

    TFile* outputFile;

    TTree* outputTreeXi;
    TTree* outputTreeXi_truth;
    TTree* outputTreeXibar;
    TTree* outputTreeXibar_truth;

    int runNumber, eventNumber;
	int m_pdgid[100];
	int m_motheridx[100];
	int m_numParticle;


    int Ntrack;
    int NProtonP;
    int NProtonM;
    int NPionM;
    int NPionP;

    double recoil1C_chisq;
    double chisq5C;

	//////////////////////////////////////////////////////////////

    int XiMatch;
	double mXip4;
    double mXi;
    double mXi_recoil1C;
	double mXiRecoil;
	double mLbd;
	double DE_Xi;

	double X2_vf1_Lbd;
	double X2_vf1_Xi;

	double X2_vf2_Lbd;
	double X2_vf2_Xi;

	double decayLength_Lbd;
	double decayError_Lbd;
	double decayLength_Xi;
	double decayError_Xi;

	double ProtonPPT;
	double ProtonPPX;
	double ProtonPPY;
	double ProtonPPZ;
	
	double PionMXiPT;
	double PionMXiPX;
	double PionMXiPY;
	double PionMXiPZ;
	
	double PionMLbdPT;
	double PionMLbdPX;
	double PionMLbdPY;
	double PionMLbdPZ;

	double XiPT;
	double XiPX;
	double XiPY;
	double XiPZ;

	double LbdPT;
	double LbdPX;
	double LbdPY;
	double LbdPZ;

	double XiVertexX;
	double XiVertexY;
	double XiVertexZ;
	double XiVertexR;

	double LbdVertexX;
	double LbdVertexY;
	double LbdVertexZ;
	double LbdVertexR;

/////////////////////////////////////////////////////////

    int XibarMatch;
	double mXibarp4;
	double mXibar;
	double mXibar_recoil1C;
	double mXibarRecoil;
	double mLbdbar;
	double DE_Xibar;

	double X2_vf1_Lbdbar;
	double X2_vf2_Lbdbar;
	double X2_vf1_Xibar;
	double X2_vf2_Xibar;

	double decayLength_Lbdbar;
	double decayError_Lbdbar;
	double decayLength_Xibar;
	double decayError_Xibar;

	double ProtonMPT;
	double ProtonMPX;
	double ProtonMPY;
	double ProtonMPZ;
	
	double PionPXiPT;
	double PionPXiPX;
	double PionPXiPY;
	double PionPXiPZ;
	
	double PionPLbdPT;
	double PionPLbdPX;
	double PionPLbdPY;
	double PionPLbdPZ;

	double XibarPT;
	double XibarPX;
	double XibarPY;
	double XibarPZ;

	double LbdbarPT;
	double LbdbarPX;
	double LbdbarPY;
	double LbdbarPZ;

	double XibarVertexX;
	double XibarVertexY;
	double XibarVertexZ;
	double XibarVertexR;

	double LbdbarVertexX;
	double LbdbarVertexY;
	double LbdbarVertexZ;
	double LbdbarVertexR;

//////////////////////////////////////////////	
	double mXip4_truth;
    double mXi_truth;
	double mXiRecoil_truth;
	double mLbd_truth;
	double DE_Xi_truth;

	double decayLength_Lbd_truth;
	double decayLength_Xi_truth;

	double ProtonPPT_truth;
	double ProtonPPX_truth;
	double ProtonPPY_truth;
	double ProtonPPZ_truth;
	
	double PionMXiPT_truth;
	double PionMXiPX_truth;
	double PionMXiPY_truth;
	double PionMXiPZ_truth;
	
	double PionMLbdPT_truth;
	double PionMLbdPX_truth;
	double PionMLbdPY_truth;
	double PionMLbdPZ_truth;

	double XiPT_truth;
	double XiPX_truth;
	double XiPY_truth;
	double XiPZ_truth;

	double LbdPT_truth;
	double LbdPX_truth;
	double LbdPY_truth;
	double LbdPZ_truth;


	double XiVertexX_truth;
	double XiVertexY_truth;
	double XiVertexZ_truth;
	double XiVertexR_truth;

	double LbdVertexX_truth;
	double LbdVertexY_truth;
	double LbdVertexZ_truth;
	double LbdVertexR_truth;
////////////////////////////////////////////////

	double mXibarp4_truth;
	double mXibar_truth;
	double mXibarRecoil_truth;
	double mLbdbar_truth;
	double DE_Xibar_truth;

	double decayLength_Lbdbar_truth;
	double decayLength_Xibar_truth;

	double ProtonMPT_truth;
	double ProtonMPX_truth;
	double ProtonMPY_truth;
	double ProtonMPZ_truth;
	
	double PionPXiPT_truth;
	double PionPXiPX_truth;
	double PionPXiPY_truth;
	double PionPXiPZ_truth;
	
	double PionPLbdPT_truth;
	double PionPLbdPX_truth;
	double PionPLbdPY_truth;
	double PionPLbdPZ_truth;

	double XibarPT_truth;
	double XibarPX_truth;
	double XibarPY_truth;
	double XibarPZ_truth;

	double LbdbarPT_truth;
	double LbdbarPX_truth;
	double LbdbarPY_truth;
	double LbdbarPZ_truth;

	double XibarVertexX_truth;
	double XibarVertexY_truth;
	double XibarVertexZ_truth;
	double XibarVertexR_truth;

	double LbdbarVertexX_truth;
	double LbdbarVertexY_truth;
	double LbdbarVertexZ_truth;
	double LbdbarVertexR_truth;

    MCTruthMatchSvc * matchSvc;
    string matchSwitch;	
    
};






#endif 
