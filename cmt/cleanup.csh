# echo "cleanup massXiAlg massXiAlg-master in /afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /afs/ihep.ac.cn/bes3/offline/ExternalLib/SLC6/contrib/CMT/v1r25
endif
source ${CMTROOT}/mgr/setup.csh
set cmtmassXiAlgtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set cmtmassXiAlgtempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt cleanup -csh -pack=massXiAlg -version=massXiAlg-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea  $* >${cmtmassXiAlgtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/mgr/cmt cleanup -csh -pack=massXiAlg -version=massXiAlg-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea  $* >${cmtmassXiAlgtempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtmassXiAlgtempfile}
  unset cmtmassXiAlgtempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtmassXiAlgtempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtmassXiAlgtempfile}
unset cmtmassXiAlgtempfile
exit $cmtcleanupstatus

