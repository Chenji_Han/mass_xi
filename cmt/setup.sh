# echo "setup massXiAlg massXiAlg-master in /afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea"

if test "${CMTROOT}" = ""; then
  CMTROOT=/afs/ihep.ac.cn/bes3/offline/ExternalLib/SLC6/contrib/CMT/v1r25; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtmassXiAlgtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then cmtmassXiAlgtempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=massXiAlg -version=massXiAlg-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea  -no_cleanup $* >${cmtmassXiAlgtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/mgr/cmt setup -sh -pack=massXiAlg -version=massXiAlg-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea  -no_cleanup $* >${cmtmassXiAlgtempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtmassXiAlgtempfile}
  unset cmtmassXiAlgtempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtmassXiAlgtempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtmassXiAlgtempfile}
unset cmtmassXiAlgtempfile
return $cmtsetupstatus

