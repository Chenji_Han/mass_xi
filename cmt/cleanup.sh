# echo "cleanup massXiAlg massXiAlg-master in /afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea"

if test "${CMTROOT}" = ""; then
  CMTROOT=/afs/ihep.ac.cn/bes3/offline/ExternalLib/SLC6/contrib/CMT/v1r25; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtmassXiAlgtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then cmtmassXiAlgtempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt cleanup -sh -pack=massXiAlg -version=massXiAlg-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea  $* >${cmtmassXiAlgtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/mgr/cmt cleanup -sh -pack=massXiAlg -version=massXiAlg-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea  $* >${cmtmassXiAlgtempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtmassXiAlgtempfile}
  unset cmtmassXiAlgtempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtmassXiAlgtempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtmassXiAlgtempfile}
unset cmtmassXiAlgtempfile
return $cmtcleanupstatus

