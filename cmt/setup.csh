# echo "setup massXiAlg massXiAlg-master in /afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /afs/ihep.ac.cn/bes3/offline/ExternalLib/SLC6/contrib/CMT/v1r25
endif
source ${CMTROOT}/mgr/setup.csh
set cmtmassXiAlgtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set cmtmassXiAlgtempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=massXiAlg -version=massXiAlg-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea  -no_cleanup $* >${cmtmassXiAlgtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/mgr/cmt setup -csh -pack=massXiAlg -version=massXiAlg-master -path=/afs/ihep.ac.cn/users/h/hancj/besfs/boss-7.0.4/workarea  -no_cleanup $* >${cmtmassXiAlgtempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtmassXiAlgtempfile}
  unset cmtmassXiAlgtempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtmassXiAlgtempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtmassXiAlgtempfile}
unset cmtmassXiAlgtempfile
exit $cmtsetupstatus

