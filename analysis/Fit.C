#include<iostream>
#include<string>
#include"bes3plotstyle.C"
using namespace RooFit;
using namespace std;

void Fit(){

	
	TFile *inputFile = new TFile("./sample/IOcheck_1.3200_precut.root");
    TTree *inputTree = (TTree*)inputFile->Get("Xi");
     
	RooRealVar mXi("mXip4","mass(#Xi^{-})/GeV",1.28,1.36);

	RooRealVar sigma2("sigma2","sigma2",0,0.1);
	RooRealVar mean2("mean2","mean2",1.3,1.33);
	RooGaussian gauss2("gauss2","gauss2",mXi,mean2,sigma2);
	
	RooRealVar sigma1("sigma1","sigma1",0, 1e-2);
	RooRealVar mean1("mean1","mean1",1.31,1.33);
	RooGaussian gauss1("gauss1","gauss1",mXi,mean1,sigma1);

	int evtNum = inputTree->GetEntries();
	RooRealVar gauss1Frac("gauss1Frac","gauss1 fraction", 0, 1);
	RooRealVar gauss2Frac("gauss2Frac","gauss2 fraction",0,1);
	RooAddPdf signal("signal","signal", RooArgList(gauss1,gauss2), gauss1Frac);
//	RooAddPdf signal("signal","signal", RooArgList(gauss1,gauss2,gauss3), RooArgList(gauss1Frac,gauss2Frac));


	//TCut cut = "(1.111<mLbd&&mLbd<1.121)";

	TFile* File = new TFile("output.root","recreate");
    //TTree* Tree = inputTree->CopyTree(cut);
	
	RooDataSet data = RooDataSet("data","data after cut",inputTree,mXi);
	
	//RooFitResult *result = gauss1.fitTo(data, Save());
	RooFitResult *result = signal.fitTo(data, Save());

	//RooDataSet* afterFit = signal.generate(mXi,1e6);
    
	TCanvas cdata;
    RooPlot* frame = mXi.frame(1.28,1.36,60) ; 
    data.plotOn(frame,MarkerColor(kBlack),MarkerStyle(8)) ; 
   // gauss1.plotOn(frame,LineColor(kBlue)) ;   
    signal.plotOn(frame,LineColor(kBlue)) ;   
    //signal.plotOn(frame,Components(gauss1),LineStyle(kDashed),LineColor(kBlue)) ;   
    //signal.plotOn(frame,Components(gauss2),LineStyle(kDashed),LineColor(kRed)) ;   

	SetStyle();
	gStyle -> SetOptStat(0);
 
	frame->GetYaxis()->SetTitle("Events"); 
	frame->GetYaxis()->SetTitleSize(0.04); 
	frame->GetXaxis()->SetTitleSize(0.04);
	frame->GetXaxis()->SetTitle("mass(#Xi^{-})");
	frame->Draw();
	string saveName = "mXip4Fit.png";
	cdata.SaveAs(saveName.c_str());

cout<<gauss1Frac.getValV()*mean1.getValV()+(1-gauss1Frac.getValV())*mean2.getValV()<<endl;
	//double meanGlobal = afterFit -> mean(mXi);
	//RooRealVar* sigmaGlobal = afterFit -> rmsVar(mXi);

	//cout<<"sigma: "<<sigmaGlobal->getValV()<<"+/- "<<sigmaGlobal->getError()<<endl;
	//cout<<"mean: "<<(afterFit->meanVar(mXi))->getValV()<<"+/-"<<(afterFit->meanVar(mXi))->getError()<<endl;
    //cout<<Tree->GetEntries()<<endl; 
}









