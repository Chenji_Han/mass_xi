

#include"bes3plotstyle.C"
using namespace RooFit;
using namespace std;

void GetShape ( string inputName ){
 

	gROOT->SetStyle("Plain");
	gStyle -> SetOptStat(0);

	TCut cut = "";
	//TCut cut = "XiMatch==1";

    TFile *inputFile = new TFile(inputName.c_str());
    cout<<"loading file "<<inputName<<endl;
	TTree *inputTree = (TTree*)inputFile->Get("Xi");

    string outputName = inputName+string("_pdf.root");
    ifstream f(outputName.c_str());
    if( f.good() ){
        cout<<"the signal shape has been extracted"<<endl;
        return;
    }
 
	TFile *outputFile = new TFile(outputName.c_str(), "recreate");
	
	TTree *tree = (TTree*)inputTree->CopyTree(cut);
	
	RooRealVar mXi("mXip4","Mass(Gev)",1.28,1.36);
	RooDataSet *data = new RooDataSet("data", "data", tree, RooArgSet(mXi));
	RooKeysPdf *sigPDF = new RooKeysPdf("sigPDF", "sigPDF", mXi, *data, RooKeysPdf::NoMirror,2);
  
	RooPlot* frame = mXi.frame();
    *sigPDF->plotOn(frame);

    TCanvas* c1 = new TCanvas("C1","C1",800,600);

	SetStyle();
	//gStyle -> SetOptStat(0);
    gPad->SetBottomMargin(0.15);
    gPad->SetLeftMargin(0.15);
    FormatAxis(frame->GetXaxis());
    FormatAxis(frame->GetYaxis());
	frame->GetYaxis()->SetTitle("Events"); 
	frame->GetXaxis()->SetTitle("PDF of mass(#Xi^{-})");
	//frame->GetYaxis()->SetTitleSize(0.04); 
	//frame->GetXaxis()->SetTitleSize(0.04);
	
    frame->Draw();

    string saveName = string("plots/mXi_pdf.png");
    c1->SaveAs(saveName.c_str());
	
	outputFile -> cd();
	sigPDF->Write();
	frame->Write();
	outputFile -> Close();
	
	return;
}
