/*************************************************************************
    > File Name: Bukin_fitting.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 09 Apr 2020 02:40:55 PM CST
 ************************************************************************/

#include"bes3plotstyle.C"

using namespace RooFit ;


double string2double(string input){
	
	int index = 0;
	double pre = 0.0;
	double aft = 0.0;
	bool comma = false;
	double num = 1.0;
	double charge;
	if(input[0]=='-') 
		charge = -1.0;
	else 
		charge = 1.0;

    bool scit = false;
    double suffix = 0;
    double factor = 0;

	while(input[index]!='\0'){
		if(input[index]=='.'){
			comma = true;
		}
		if('0'<=input[index]&&input[index]<='9'){
			double temp = input[index] - '0';
			if(comma==false){
				pre = pre*10.0 + temp;
			}else{
				num *= 10.0;
				aft += temp/num;
			}
		}
        if(input[index]=='e' || input[index] == 'E'){
            scit = true;
            suffix = pre + aft;
            factor = string2double( input.substr( size_t(index+1) ) );
            break; 
        }
		index++;
	}

    if(scit)
	    return charge * suffix * pow( 10, factor ) ;
    else
        return charge * ( pre + aft) ;
}

int readTxt (double* N) {


    string a;
    int ievt = 0;
    std::fstream inputTxtFile(".BukinExtractionMC.txt",ios::in);
    string line;
    while(std::getline(inputTxtFile,line)){
        if( !line.length() || line[0]=='#' ){
            continue;
        }
        std::istringstream iss(line);
        iss>>a;
        N[ievt++] = string2double(a);
        if(ievt>5) break;
    }

    return ievt;
}


void Bukin_fitting(string inputName=string("sample/Jpsi5C.root"),string flag=string("data5C")){

	TFile *inputFile = new TFile(inputName.c_str());
    TTree *tree = (TTree*)inputFile->Get("Xi");
 	
	RooRealVar mXi("mXip4","",1.30,1.34);

    double N[12];
    readTxt(N);

    RooRealVar _Xp("_Xp","",N[0]) ;
    RooRealVar _sigp("_sigp","",N[1]) ;
    RooRealVar _xi("_xi","",N[2]) ;
    RooRealVar _rho1("_rho1","",N[3]) ;
    RooRealVar _rho2("_rho2","",N[4]) ; 

    RooRealVar shift("shift","shift",0,0.002);

    RooFormulaVar _Xp_shift("_Xp_shift","_Xp+shift",RooArgSet(_Xp,shift));

    RooBukinPdf sigMC("sigMC","",mXi, _Xp_shift, _sigp, _xi, _rho1, _rho2 );

    RooRealVar mean_conv("mean_conv", "mean",0.0);

	RooRealVar sigma1_conv("sigma1_conv", "sigma",1e-4,1e-2);
	RooGaussian gauss1_conv("gauss1_conv", "gauss", mXi, mean_conv, sigma1_conv);
    
	RooFFTConvPdf sig("sig", "", mXi, sigMC, gauss1_conv );

    RooRealVar a1_bkg("a1_bkg","",-200,200);
    RooRealVar a2_bkg("a2_bkg","",-200,200);
	RooChebychev bkg("background","background",mXi,RooArgList(a1_bkg,a2_bkg) );

	int nentr = tree->GetEntries();
	RooRealVar nsig("nsig", "nsig",0, nentr);
	RooRealVar nbkg("nbkg", "nbkg",0, nentr);

	RooAddPdf model("pdf", "pdf", RooArgList(sig,bkg), RooArgList(nsig,nbkg) );
    
    RooDataSet data("data","data",tree,mXi);

    RooFitResult * result = model.fitTo(data,Save(kTRUE));

    TCanvas* C = new TCanvas("C","C",800,600);
    SetStyle();

    gPad->SetLeftMargin(0.18);
    gPad->SetBottomMargin(0.13);
    RooPlot* frame = mXi.frame(1.3,1.34); 
    //RooPlot* frame = mXi.frame(1.3,1.34,150); 

	data.plotOn(frame,MarkerSize(0.6)); 
	model.plotOn(frame,Components(sig),LineStyle(kDashed),LineColor(kRed),LineWidth(3) );   
    model.plotOn(frame,Components(bkg),LineStyle(kDashed),LineColor(kYellow),LineWidth(3) ) ;   
    model.plotOn(frame,LineWidth(3),LineColor(kBlue)) ;   

    FormatAxis(frame->GetYaxis());
    FormatAxis(frame->GetXaxis());
    frame->GetYaxis()->SetRangeUser(0.01,frame->GetMaximum()*1.15);
    frame->GetXaxis()->SetTitle("mass(#Xi^{-})/GeV");
    frame->GetYaxis()->SetTitle("Events");
    frame->GetYaxis()->SetLabelSize(0.03);
    frame->GetYaxis()->SetTitleSize(0.04);
    frame->GetXaxis()->SetLabelSize(0.03);
    frame->GetXaxis()->SetTitleSize(0.04);
	frame->Draw();
    
    string saveName = string("./plots/") + flag + string(".png");
    cout<<saveName<<endl;
	C->SaveAs(saveName.c_str());
    
    TCanvas* C2 = new TCanvas("C2","C2",800,600);
    frame->GetYaxis()->SetRangeUser(0.1,1e4);
    gPad->SetLogy();
    frame -> Draw();
    string saveName2 = string("./plots/") + flag + string("_logy.png");
    cout<<saveName2<<endl;
    C2 -> SaveAs(saveName2.c_str());
    
    ofstream outputTxtFile("Bukin_fitting_output.txt",ios::app);
    if( !outputTxtFile ){
        cout<<" Error in opening output.txt " <<endl;
        return;
    }

    string tag = inputName;
  
   	outputTxtFile<<scientific<<tag<<"  "<<shift.getValV()+1.32132<<"  "<<shift.getError()<<"  ";

    Int_t ndf = result->floatParsFinal().getSize();
	Double_t chisq = frame->chiSquare();
	Double_t reduced_chisq = frame->chiSquare(ndf);
	outputTxtFile << "NDF:" << ndf << endl;
	outputTxtFile << "chi square: " << chisq <<endl;
	outputTxtFile << "reduced chi square: " << reduced_chisq << endl;
    outputTxtFile.close();


}
