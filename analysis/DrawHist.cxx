/*************************************************************************
    > File Name: DrawHist.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Sun 29 Mar 2020 03:29:22 PM CST
 ************************************************************************/

#include"bes3plotstyle.C"

void draw(TH1D* H,string titleName,string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H);
    TCanvas C("C","",800,600);
	H -> GetXaxis() -> SetTitle(titleName.c_str());
	H -> Draw("hist");
    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	C.SaveAs(plotName2.c_str());


}

void DrawHist() {

<<<<<<< HEAD
    TFile inputFile("sample/signalMC5C.root");
    TTree* inputTree = (TTree*) inputFile.Get("Xi");
    
    TH1D H0("H0","",100,1.28,1.36);
    inputTree -> Project("H0","mXip4");
    draw(&H0, "M(#Xi^{-})", "mxi5c");


    //TH1D H1("H1","",500,0,500);
    //inputTree -> Project("H1","chisq4C");
    //draw(&H1, "#chi^{2} of 5C", "chisq5C");

   // TH1D H2("H2","",100,1.31,1.335);
   // inputTree -> Project("H2","mXip4","mXiRecoil>1.28&&mXiRecoil<1.35&&mLbd>1.111&&mLbd<1.121");
   // draw(&H2, "mass of #Xi", "mXi");

=======
    TFile inputFile("sample/signalMC_precut.root");
    TTree* inputTree = (TTree*) inputFile.Get("Xi");
    
   //TH1D H1("H1","",100,1.24,1.40);
   //inputTree -> Project("H1","mXiRecoil","mLbd>1.111&&mLbd<1.121");
   //draw(&H1, "mass of the recoil side of #Xi", "mXiRecoil");

   // TH1D H2("H2","",100,1.31,1.335);
   // inputTree -> Project("H2","mXip4","mXiRecoil>1.28&&mXiRecoil<1.35&&mLbd>1.111&&mLbd<1.121");
   // draw(&H2, "mass of #Xi", "mXi");

>>>>>>> parent of 5782c71... add lambda mass distri check
   // TH1D H3("H3","",100,-10,20);
   // inputTree -> Project("H3","decayLength_Xi","mXiRecoil>1.28&&mXiRecoil<1.35&&mLbd>1.111&&mLbd<1.121");
   // draw(&H3, "decay length of #Xi", "decayLength_Xi");

<<<<<<< HEAD
    //TH1D H4("H4","",100,1.111,1.121);
    //inputTree -> Project("H4","mLbd");
    //draw(&H4, "mass of #Lambda", "mLbd");

    //TH1D H5("H5","",100,-10,25);
    //inputTree -> Project("H5","decayLength_Lbd","mXiRecoil>1.28&&mXiRecoil<1.35&&mLbd>1.111&&mLbd<1.121");
    //draw(&H5, "decay length of #Lambda", "decayLength_Lbd");

    TH1D H6("H6","",100,0,20);
    inputTree -> Project("H6","X2_vf1_Lbd");
    draw(&H6, " #chi^{2} of vertex fit of #Lambda", "data_chi2Lbd");
=======
   // TH1D H4("H4","",100,1.111,1.121);
   // inputTree -> Project("H4","mLbd","mXiRecoil>1.28&&mXiRecoil<1.35&&mLbd>1.111&&mLbd<1.121");
   // draw(&H4, "mass of #Lambda", "mLbd");

   // TH1D H5("H5","",100,-10,25);
   // inputTree -> Project("H5","decayLength_Lbd","mXiRecoil>1.28&&mXiRecoil<1.35&&mLbd>1.111&&mLbd<1.121");
   // draw(&H5, "decay length of #Lambda", "decayLength_Lbd");

>>>>>>> parent of 5782c71... add lambda mass distri check

    cout<<inputTree->GetEntries("mXiRecoil>1.28&&mXiRecoil<1.35&&mLbd>1.111&&mLbd<1.121")<<endl;

}


