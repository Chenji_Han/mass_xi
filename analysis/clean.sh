#########################################################################
# File Name: clean.sh
# Author: Chenji Han
# mail: hanchenji16@mails.ucas.ac.cn
# Created Time: Wed 04 Mar 2020 10:50:39 AM CST
#########################################################################
#!/bin/bash




rm -f *.boss*
rm -f *.out.*
rm -f *.err.*
rm -f *test*root
