/*************************************************************************
    > File Name: compare.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Mon 13 Jan 2020 09:38:33 AM CST
 ************************************************************************/

#include"bes3plotstyle.C"
#include"LibChenji.cxx"

void compare_sub(TH1D* Hreco,TH1D* Htruth,string titleName,int binNum,double axisMin,double axisMax,string storeName){

    // SetStyle();

    TCanvas* c=new TCanvas("c","",800,600);
    SetStyle();

    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.35, 1, 1.0);
    pad1->SetBottomMargin(0); // Upper and lower plot are joined
    pad1->SetGridx();         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    
    pad1->cd();               // pad1 becomes the current pad
    //SetStyle();
    Hreco->SetStats(0);          // No statistics on upper plot
    Htruth->SetStats(0);          // No statistics on upper plot
    // gPad->SetOptTitle(0);
    //gPad->SetTitle("");
    Htruth->SetMarkerSize(0);
    Hreco->SetMarkerSize(0);
     
    Htruth->Scale(1.0/Htruth->Integral());
    Hreco->Scale(1.0/Hreco->Integral());

    TLegend* legend = new TLegend(0.60,0.67,0.90,0.87);
<<<<<<< HEAD
<<<<<<< HEAD
    legend -> AddEntry(Hreco,"M(#Xi) data");
    legend -> AddEntry(Htruth,"M(#Xi) signal MC");
=======
    legend -> AddEntry(Hreco,"M(#Xi) 5C");
    legend -> AddEntry(Htruth,"M(#Xi) no 5C");
>>>>>>> parent of e3ff12e... no boost in truth info
   
=======
    legend -> AddEntry(Hreco,"Han, Chenji");
    legend -> AddEntry(Htruth,"Gao, Han");
    
    //legend -> AddEntry(Hreco,"Han, Chenji");
    //legend -> AddEntry(Htruth,"Gao, Han");
    
    //legend -> AddEntry(Hreco,"Han, Chenji");
    //legend -> AddEntry(Htruth,"Gao, Han");
>>>>>>> parent of 5782c71... add lambda mass distri check
    legend->SetFillColor(0);
    
    FormatAxis(Hreco->GetYaxis());
    FormatAxis(Hreco->GetXaxis());
    Hreco->GetYaxis()->SetTitleOffset(0.8);
 
    Hreco->GetYaxis()->SetTitle("Events");
   	
    Hreco->GetXaxis()->SetTitle("");
   	// Hreco->GetXaxis()->SetTitleOffset();
   	Hreco->GetXaxis()->SetTitleSize(0.08);
    Hreco->SetMaximum(Hreco->GetMaximum()*1.2);
    Hreco->GetYaxis()->SetRangeUser(0.001, Hreco->GetMaximum()*1.1);
    //hsig->SetMinimum(1);
    ////gPad->SetLogy();
    TGaxis* xaxis = (TGaxis*)Hreco->GetYaxis();
    xaxis->SetMaxDigits(3);

    Hreco->SetLineColor(kBlue+1);
   	Hreco->SetLineWidth(2);

   	// hsig2 settings
   	Htruth->SetLineColor(kRed);
   	Htruth->SetLineWidth(2);
<<<<<<< HEAD
    //Htruth->Scale(Hreco->Integral()/Htruth->Integral());
    
=======
  //  Htruth->Scale(Hreco->Integral()/Htruth->Integral());

>>>>>>> parent of 5782c71... add lambda mass distri check
    Hreco->Draw("HIST");
    Htruth->Draw("same HIST");
    legend -> Draw("same");
    
    pad1->Update();

	// lower plot will be in pad
   	c->cd();          // Go back to the main canvas before defining pad2
    SetStyle();
   	TPad *pad2 = new TPad("pad2", "pad2", 0, 0.0, 1, 0.35);
   	pad2->SetTopMargin(0.001);
   	pad2->SetBottomMargin(0.4);
   	pad2->SetGridx(); // vertical grid
   	pad2->Draw();
   	pad2->cd();       // pad2 becomes the current pad

	// Define the ratio plot
    Double_t binerr;
    TH1F* h3 = new TH1F("h3","h3",binNum,axisMin,axisMax);
    TH1F* h4 = new TH1F("h4","h4",binNum,axisMin,axisMax);
    h3->Add(Hreco,Htruth,1,-1);
    for (int i=1; i<=binNum; i++){
        binerr = Hreco->GetBinError(i);
        if (binerr!=0){
            h4->SetBinContent(i, binerr);
        }
    }
    h3->Divide(h3,h4);
    for (int j=1;j<binNum+1;j++){
        h3->SetBinError(j, 1.0);
    }
   	//TH1F *h3 = (TH1F*)hsig->Clone("h3");
   	h3->SetLineColor(kBlack);
   	h3->SetMinimum(-9.9);  // Define Y ..
   	h3->SetMaximum(9.09); // .. range
   	
    h3->SetMarkerStyle(21);

   	// hsig settings
   	// Ratio plot (h3) settings
   	h3->GetXaxis()->SetTitle(titleName.c_str()); 


   	// Y axis ratio plot settings
   	h3->GetYaxis()->SetTitle("#chi");
    h3->SetMarkerSize(0.5);
    FormatAxis(h3->GetYaxis());
    FormatAxis(h3->GetXaxis());
   	h3->GetYaxis()->SetTitleSize(0.13);
   	h3->GetYaxis()->SetLabelSize(0.13); 
   	h3->GetXaxis()->SetLabelSize(0.13); 
    h3->GetXaxis()->SetTitleSize(0.15);
   	h3->GetYaxis()->SetTitleOffset(0.3);
   	h3->GetXaxis()->SetTitleOffset(1.24);
   
   	h3->Draw("E1");       // Draw the ratio plot

    //hsig->Draw("E");
    //hsig2->Draw("same:E");
    //TLegend *leg = new TLegend(0.58,0.7,0.83,0.8 );
    //leg->AddEntry(hsig,"Data");
    //Format(leg);
    //leg->Draw();
    // if you want draw an arrow , add the following statements
    //TArrow *arr = new TArrow(0,1000,0,0,0.01,">");
    //Format(arr);
    //arr->Draw(); 
    
    c->SaveAs((string("./plots/")+storeName+string(".png")).c_str());
    c->SaveAs((string("./plots/")+storeName+string(".eps")).c_str());

    delete h3;
    delete h4;
    delete c;

    return;
}

void compare(){
        
    SetStyle();
    gStyle->SetPadRightMargin(0.15);

    vector<string> Name1;
    vector<string> Name2;
    vector<int> Bin;
    vector<double> Min;
    vector<double> Max;

    string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c,d,e,f;
        iss>>a>>b>>c>>d>>e>>f;

		if(a=="compare"){
            Name1.push_back(b);
            Name2.push_back(c);
            Bin.push_back(string2double(d));
            Min.push_back(string2double(e));
            Max.push_back(string2double(f));
        }
	}

    TFile* inputFile1 = new TFile("./sample/Jpsi.root");
    TTree *inputTree1 = (TTree*) inputFile1->Get("Xi");
    TFile* inputFile2 = new TFile("./sample/Jpsi5C.root");
    TTree *inputTree2 = (TTree*) inputFile2->Get("Xi");

    TH1D* H1 = new TH1D("H1","",100,1.28,1.36); 
    TH1D* H2 = new TH1D("H2","",100,1.28,1.36); 

    inputTree1 -> Project("H1","mXip4","mXiRecoil>1.30&&mXiRecoil<1.34&&mLbd>1.111&&mLbd<1.121"); 
    inputTree2 -> Project("H2","mXip4"); 

    compare_sub(H2,H1,"mass of #Xi",100,1.28,1.36,"ori_5C_data");
    
    //TFile* inputFile1 = new TFile("/besfs/users/gaohan/7.0.4/test_Xim_corr.root");
    //TTree *inputTree1 = (TTree*) inputFile1->Get("tree");
    //TFile* inputFile2 = new TFile("test.root");
    //TTree *inputTree2 = (TTree*) inputFile2->Get("Xi");
    //
    //for(int i=0;i<Name1.size();i++){
    //    cout<<Name1[i]<<endl;

    //    TH1D* H_Gao = new TH1D("Gao","",Bin[i],Min[i],Max[i]); 
    //    TH1D* H_Han = new TH1D("Han","",Bin[i],Min[i],Max[i]); 

    //    inputTree1 -> Project("Gao",Name1[i].c_str(),"mXi>1.28&&mXi<1.36&&charge==-1&&mXi_recoil>1.28&&mXi_recoil<1.36&&mLbd>1.111&&mLbd<1.121"); 
    //    inputTree2 -> Project("Han",Name2[i].c_str(),"mXip4>1.28&&mXip4<1.36&&mXiRecoil>1.28&&mXiRecoil<1.36&&mLbd>1.111&&mLbd<1.121"); 
 
    //    compare_sub(H_Han,H_Gao,Name2[i],Bin[i],Min[i],Max[i],Name2[i]);

    //    delete H_Gao;
    //    delete H_Han;
    //}

}



