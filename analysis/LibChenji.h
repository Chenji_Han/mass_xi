/*************************************************************************
    > File Name: functions.h
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Tue 16 Jul 2019 07:54:38 PM CST
 ************************************************************************/



double mpion    = 0.13957;
double mpi      = 0.13957;
double mk       = 0.493677;
double mp       = 0.938272;
double mproton  = 0.938272;


bool IsOverlap(vector<double>, vector<int>*);

double string2double(string );

double Get_Mbc(TLorentzVector );

bool bkg_veto(int,int,vector<vector<double> >*,vector<double>,double,double);

bool bkg_veto(int,double,int,double,vector<vector<double> >*,vector<double>,double,double);

bool bkg_veto(int,double,vector<vector<double> >,vector<double>*,double,double);

bool bkg_veto(int,double,int,double,vector<vector<double> >*,double,double);

