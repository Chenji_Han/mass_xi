
#include"bes3plotstyle.C"

using namespace RooFit ;


double string2double(string input){
	
	int index = 0;
	double pre = 0.0;
	double aft = 0.0;
	bool comma = false;
	double num = 1.0;
	double charge;
	if(input[0]=='-') 
		charge = -1.0;
	else 
		charge = 1.0;

    bool scit = false;
    double suffix = 0;
    double factor = 0;

	while(input[index]!='\0'){
		if(input[index]=='.'){
			comma = true;
		}
		if('0'<=input[index]&&input[index]<='9'){
			double temp = input[index] - '0';
			if(comma==false){
				pre = pre*10.0 + temp;
			}else{
				num *= 10.0;
				aft += temp/num;
			}
		}
        if(input[index]=='e' || input[index] == 'E'){
            scit = true;
            suffix = pre + aft;
            factor = string2double( input.substr( size_t(index+1) ) );
            break; 
        }
		index++;
	}

    if(scit)
	    return charge * suffix * pow( 10, factor ) ;
    else
        return charge * ( pre + aft) ;
}

int readTxt (double* N) {


    string a;
    int ievt = 0;
    std::fstream inputTxtFile(".MultiGExtraction.txt",ios::in);
    string line;
    while(std::getline(inputTxtFile,line)){
        if( !line.length() || line[0]=='#' ){
            continue;
        }
        std::istringstream iss(line);
        iss>>a;
        N[ievt++] = string2double(a);
        if(ievt>12) break;
    }

    return ievt;
}


void Fit_multiG(string inputName=string("sample/Jpsi5C.root"),string flag=string("data5C")){

	TFile *inputFile = new TFile(inputName.c_str());
    TTree *tree = (TTree*)inputFile->Get("Xi");
 	
	RooRealVar mXi("mXip4","",1.31,1.335);

    double N[12];
    readTxt(N);

    RooRealVar mean1("mean1","mean of gaussian",N[0]) ;
    RooRealVar mean2("mean2","mean of gaussian",N[1]) ;
    RooRealVar mean3("mean3","mean of gaussian",N[2]) ;
    RooRealVar mean4("mean4","mean of gaussian",N[3]) ;
    RooRealVar sigma1("sigma1","sigma core",N[4]) ;
    RooRealVar sigma2("sigma2","sigma tail",N[5]); 
    RooRealVar sigma3("sigma3","sigma tail",N[6]); 
    RooRealVar sigma4("sigma4","sigma tail",N[7]); 
    RooRealVar frac1("frac1","fraction",N[8]) ; 
    RooRealVar frac2("frac2","fraction",N[9]) ; 
    RooRealVar frac3("frac3","fraction",N[10]) ; 
    RooRealVar frac4("frac4","fraction",N[11]) ; 

    RooRealVar shift("shift","shift",0,0.002);

    RooFormulaVar mean1_shift("mean1_shift","mean1+shift",RooArgSet(mean1,shift));
    RooFormulaVar mean2_shift("mean2_shift","mean2+shift",RooArgSet(mean2,shift));
    RooFormulaVar mean3_shift("mean3_shift","mean3+shift",RooArgSet(mean3,shift));
    RooFormulaVar mean4_shift("mean4_shift","mean4+shift",RooArgSet(mean4,shift));

    RooGaussian gauss1("gauss1","gauss",mXi,mean1_shift,sigma1);
    RooGaussian gauss2("gauss2","gauss",mXi,mean2_shift,sigma2);
    RooGaussian gauss3("gauss3","gauss",mXi,mean3_shift,sigma3);
    RooGaussian gauss4("gauss4","gauss",mXi,mean4_shift,sigma4);

    RooAddPdf sigMC("sigMC","sigMC",RooArgList(gauss1,gauss2,gauss3,gauss4),RooArgList(frac1, frac2, frac3, frac4));

    RooRealVar mean_conv("mean_conv", "mean",0.0);

	RooRealVar sigma1_conv("sigma1_conv", "sigma",1e-6,1e-3);
	RooGaussian gauss1_conv("gauss1_conv", "gauss", mXi, mean_conv, sigma1_conv);
    
	RooFFTConvPdf model("model", "", mXi, sigMC, gauss1_conv );

    //RooRealVar a1_bkg("a1_bkg","",-2000,2000);
    ////RooRealVar a2_bkg("a2_bkg","",-2000,2000);
	//RooPolynomial bkg("background","background",mXi,RooArgList(a1_bkg) );

	//int nentr = tree->GetEntries();
	//RooRealVar nsig("nsig", "nsig",0, nentr);
	//RooRealVar nbkg("nbkg", "nbkg", 0.01*nentr, 0, nentr);

	//RooAddPdf model("pdf", "pdf", RooArgList(sig,bkg), RooArgList(nsig,nbkg) );
    
    RooDataSet data("data","data",tree,mXi);

    RooFitResult * result = model.fitTo(data,Save(kTRUE), Extended() );

    TCanvas* C = new TCanvas("C","C",800,600);
    SetStyle();

    gPad->SetLeftMargin(0.18);
    gPad->SetBottomMargin(0.13);
    RooPlot* frame = mXi.frame(1.31,1.335,100); 

	data.plotOn(frame,MarkerSize(0.6)); 
	//model.plotOn(frame,Components(sig),LineStyle(kDashed),LineColor(kRed),LineWidth(3));   
    model.plotOn(frame,LineWidth(3),LineColor(kBlue)) ;   

    FormatAxis(frame->GetYaxis());
    FormatAxis(frame->GetXaxis());
    frame->GetXaxis()->SetTitle("mass(#Xi^{-})/GeV");
    frame->GetYaxis()->SetTitle("Events");
    frame->GetYaxis()->SetLabelSize(0.03);
    frame->GetYaxis()->SetTitleSize(0.04);
    frame->GetXaxis()->SetLabelSize(0.03);
    frame->GetXaxis()->SetTitleSize(0.04);
	frame->Draw();
    
    string saveName = string("./plots/") + flag + string(".png");
    cout<<saveName<<endl;
	C->SaveAs(saveName.c_str());
    
    TCanvas* C2 = new TCanvas("C2","C2",800,600);
    gPad->SetLogy();
    frame -> Draw();
    string saveName2 = string("./plots/") + flag + string("_logy.png");
    cout<<saveName2<<endl;
    C2 -> SaveAs(saveName2.c_str());
    
    ofstream outputTxtFile("MultiGoutput.txt",ios::app);
    if( !outputTxtFile ){
        cout<<" Error in opening output.txt " <<endl;
        return;
    }

    string tag = inputName;
  
   	outputTxtFile<<scientific<<tag<<"  "<<shift.getValV()+1.32132<<"  "<<shift.getError()<<"  ";

    Int_t ndf = result->floatParsFinal().getSize();
	Double_t chisq = frame->chiSquare();
	Double_t reduced_chisq = frame->chiSquare(ndf);
	outputTxtFile << "NDF:" << ndf << endl;
	outputTxtFile << "chi square: " << chisq <<endl;
	outputTxtFile << "reduced chi square: " << reduced_chisq << endl;
    outputTxtFile.close();


}
